
#define MTI_ALL       0xAA
#define MTI_ARM       0xAB    // for ARM scenarios only!
#define MTI_SHUTTERS  0xAC
#define MTI_LIGHTS    0xAD

#define MTI_EXVA      0xBB

#define MTI_4SEC      0x31
#define MTI_4INTR     0x32
#define MTI_16KEY     0x33

#define MTI_DRV       0x40    // bus driver (because of SOFT VERSION)
#define MTI_8OUT      0x41
#define MTI_8IN       0x42
#define MTI_2OUT      0x43
#define MTI_1SHU      0x44
#define MTI_1DIM      0x45
#define MTI_FTC232    0x46    // also for touch screen
#define MTI_MODEM     0x47    // CENTRALSEC
#define MTI_26IN      0x48
#define MTI_4OUT      0x49
#define MTI_H3IN      0x4A
#define MTI_RXRF      0x4B
#define MTI_4IN       0x4C
#define MTI_9KEY      0x4D
#define MTI_7KEY      0x4E
#define MTI_ACCTL     0x4F
#define MTI_1DIM010V  0x50
#define MTI_DMS        0x51
#define MTI_9KEY_TABLE 0x52
#define MTI_CLIMA      0x53
#define MTI_TERMO      0x54
#define MTI_D4SHU      0x55
#define MTI_4DET       0x56
#define MTI_FM2AU      0x57
#define MTI_1OUT       0x58
#define MTI_HTLCD      0x59
#define MTI_8OUT3IN    0x5A
#define MTI_2MOT       0x5B
#define MTI_FTC2E      0x5C
#define MTI_FM1AU      0x5D
#define MTI_DIMRGB     0x5E

