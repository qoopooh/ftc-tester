#include "database.h"
#include <QDebug>

Database::Database(QObject *parent) : QObject(parent),
  host("192.168.1.15"), dbname("domotics"), user("berm"), pass("berm")
{
  db = QSqlDatabase::addDatabase("QMYSQL");
  db.setHostName(host);
  db.setDatabaseName(dbname);
  db.setUserName(user);
  db.setPassword(pass);
}

bool Database::isDuplicatedSerial(const QString &sn)
{
  bool ok = db.open();

  if (!ok) {
    emit error(CannotOpen, tr("Cannot open db"));
    return false;
  }

  QSqlQuery query("SELECT serial FROM command");
  ok = false;
  while (query.next()) {
    if (query.value(0).toString().contains(sn)) {
      ok = true;
      break;
    }
  }
  if (ok)
    qDebug() <<  QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss")
              << sn << "was checked";
  db.close();
  return ok;
}

/**
 * @brief Database::isTestedSerial
 * @param sn serial number
 * @param result last result
 * @return tested status
 */
bool Database::isTestedSerial(const QString &sn, int *result)
{
  bool ok = db.open();
  quint32 id(0);
  QString q("SELECT id, serial FROM command");
  QString tested_date;

  if (!ok) {
    emit error(CannotOpen, tr("Cannot open db"));
    return false;
  }

  QSqlQuery query(q);
  ok = false;
  while (query.next()) {
    if (query.value(1).toString().contains(sn)) {
      id = query.value(0).toInt();
      ok = true;
      break;
    }
  }
  if (!ok) {
    db.close();
    return false;
  }
  q = tr("SELECT result, date FROM test WHERE command_id=")
      + QString::number(id) + " ORDER BY date DESC;";
  query.exec(q);
  ok = false;
  if (query.next()) {
    if (result != NULL) {
      *result = query.value(0).toInt();
    }
    tested_date =  query.value(1).toString();
    ok = true;
  }
  if (ok)
    qDebug() <<  QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss")
              << sn << "was tested on " + tested_date;
  db.close();
  return ok;
}

bool Database::addCommand(const QString &sn)
{
  bool ok(false);

  if (sn.length() != 8) {
    qDebug() << "Serial number size is not correct";
    return false;
  }

  if (isDuplicatedSerial(sn))
    return true;
  ok = db.open();
  if (!ok) {
    emit error(CannotOpen, tr("Cannot open db"));
    return false;
  }

  QString q("INSERT INTO command (serial, date) VALUES ('" + sn + "','"
            + QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss")
            + "');");
  QSqlQuery query;
  ok = query.exec(q);
  if (!ok)
    qDebug() << "cannot addCommand";
  db.close();

  return ok;
}

/**
 * @brief Database::commitResult send test result to database
 * @param sn serial number
 * @param version firmware version
 * @param result test result
 * @param thermo_type indentify unit in thermo device group
 * @param dimrgb_type indentify unit in rgb dimmer device group
 * @return success status
 */
bool Database::commitResult(const QString &sn,
                            const QString &version, int result,
                            ThermoDeviceType &thermo_type,
                            DimRgbDeviceType &dimrgb_type)
{
//  return true;
  bool ok = db.open();

  if (!ok) {
    emit error(CannotOpen, tr("Cannot open db"));
    return false;
  }

  if ((sn == "") || (sn == "-"))
    return false;

  QString type("-");
  QString mti = sn.mid(0, 2);
  if (mti == "54") {
    type = thermo_type.toString();
  } else if (mti == "5E") {
    // W-1DIMRGB, D-DIMRGB
    type = dimrgb_type.toString();
  }

  QString q("SELECT id FROM command WHERE serial='" + sn + "';");
  QSqlQuery query(q);
  int id(0);

  if (query.next()) {
    id = query.value(0).toInt();
  } else {
    addCommand(sn);
    query.exec();
    if (query.next()) {
      id = query.value(0).toInt();
    } else {
      emit error(CannotSelect, QString("Cannot find serial: %1\nOr database connnection error").arg(sn));
      return false;
    }
  }
  qDebug() << "commitResult" << q;
  q.clear();
  q = tr("INSERT INTO test (command_id, version, result, type, date) VALUES (")
      + QString::number(id) + ",'" + version + "',"
      + QString::number(result) + ",'"
      + type + "','"
      + QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss") + "');";
  qDebug() << "commitResult" << q;
  //return true;
  ok = query.exec(q);
  if (!ok)
    emit error(CannotInsert, tr("Cannot commit result"));

  db.close();

  return ok;
}

QString kReport("SELECT command.serial, t1.version, t1.result, t1.date, t1.type"
                 " FROM command, test t1"
                 " WHERE command.id=t1.command_id");
QString kReportLast("SELECT serial, t1.version, t1.result, t1.date, t1.type"
           " FROM test t1 LEFT JOIN test t2"
           " ON (t1.command_id=t2.command_id AND t1.id < t2.id), command"
           " WHERE t2.id IS NULL"
           " AND t1.command_id=command.id");
QString kOrder(" ORDER BY t1.date DESC");

/**
 * @brief Database::createReport Create report separate value by tab
 * @param start starting date
 * @param finish finishing date
 * @param mti group of product
 * @param f flag to show only last result of each unit id
 * @return
 */
QString Database::createReport(const QString start, const QString finish,
                               const QString mti, const char f)
{
  bool ok = db.open();

  if (!ok) {
    emit error(CannotOpen, tr("Cannot open db"));
    return "";
  }

  QString q(kReport);

  if (f == 'l') //last test result of each unit
    q = kReportLast;
  if (start != "") {
    q += " AND t1.date >= '" + start + "'";
    if (finish != "") {
      q += " AND t1.date < '" + finish + "'";
      if (mti != "") {
        q += " AND command.serial LIKE '" + mti + "%'";
      }
    }
  }
  int col_number=4;
  if((mti=="")||(mti==QString::number(MTI_TERMO, 16).toUpper())||(mti==QString::number(MTI_DIMRGB, 16).toUpper())){
      col_number=5;
  }
  q += kOrder;
  qDebug() << "report" << q;

  QSqlQuery query(q);
  int no(0);
  QString result;

  while (query.next()) {
    result += QString::number(++no);
    for (int i = 0; i < col_number; ++i) {
      result += "\t" + query.value(i).toString();
    }
    result += "\n";
  }
  qDebug() << result;

  db.close();
  return result;
}

