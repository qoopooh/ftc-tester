#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDateTime>

#include "tester.h"
#include "thermodevice.h"
#include "thermodevicetype.h"
#include "dimrgbdevice.h"
#include "dimrgbdevicetype.h"

class Database : public QObject
{
  Q_OBJECT
public:
  explicit Database(QObject *parent = 0);

  bool isTestedSerial(const QString &, int *result = NULL);
  bool addCommand(const QString &);
  bool commitResult(const QString &, const QString &, int result,
                    ThermoDeviceType &thermo_type,DimRgbDeviceType &);
  QString createReport(const QString start="", const QString finish="",
                       const QString mti="", const char f='a');

  typedef enum {
    CannotOpen,
    CannotSelect,
    CannotInsert,
    WrongFormat,
    ErrorEtc
  } Error;

signals:
  void error(int, QString);

public slots:

private:
  bool isDuplicatedSerial(const QString &);

  QSqlDatabase db;
  QString host;
  QString dbname;
  QString user;
  QString pass;
};

#endif // DATABASE_H
