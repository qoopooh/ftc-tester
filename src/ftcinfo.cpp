#include "ftcinfo.h"
#include <QDebug>

FtcInfo::FtcInfo(QObject *parent) :
  QObject(parent), cmd(0x00), mti(0x00)
{
}

FtcInfo::~FtcInfo()
{
}

void FtcInfo::setInfo(const QByteArray &ba)
{
  msg_type =  ba[1] & CONFIRMATION;
  cmd = ba.at(2);
  if (cmd & 0x80) {
    cmd &= ~(0x80);
    f_cmd_bit8 = true;
  } else {
    f_cmd_bit8 = false;
  }
  switch (cmd) {
    case CMD_TIME_DATE:
      setAddress(ba.mid(11, 4));
      setParam(ba.mid(3, 8));
      emit time(toTime());
      break;
    case CMD_CLIMA:
      setAddress(ba.mid(3, 4));
      setParam(ba.mid(7, 8));
      emit temperature(toDegree());
      break;
    case CMD_STATUS:
      setAddress(ba.mid(3, 4));
      setParam(ba.mid(7, ba.length() - 9));
      debugCommand();
      emitStatus();
      break;
    case CMD_SET:
    case CMD_RESET:
    case CMD_TOGGLE:
      setAddress(ba.mid(3, 4));
      setParam(ba.mid(7, ba.length() - 9));
      debugCommand();
      break;
    case CMD_TOGGLE_MASTER: // for PROG Key
      setAddress(NULL);
      setParam(ba.mid(3, 1));
      emit event(tr("PROG: ") + QString::number(param[0]));
      break;
    case CMD_SOFT_VERSION: // for PROG Key
      if (msg_type != INDICATION)
        break;
      setAddress(ba.mid(3, 4));
      setParam(ba.mid(7, ba[0] - 8));
      emit event(tr("Version: ") + param.data());
      break;
    case CMD_ADDRESS:
    default:
      setAddress(ba.mid(3, 4));
      setParam(NULL);
      debugCommand();
      break;
  }
//  qDebug() << "Param size" << param.length();
}

void FtcInfo::setAddress(const QByteArray &ba)
{
  mti = 0;
  if (!ba.isNull()) {
    addr.clear();
    addr = ba;
    mti = ba[0];
    product_lot = ba[1];
    serial = (ba[2] << 8) + ba[3];
  }
  f_addr_set = true;
}

void FtcInfo::setParam(const QByteArray &ba)
{
  if (ba.isNull()) {
    param.clear();
  } else {
    param = ba;
  }

  if (f_addr_set) {
    emit address(getAddress());
    f_addr_set = false;
  }
}

void FtcInfo::emitStatus()
{
  QString status("Status: ");
  int val(0);

  switch (mti) {
    case MTI_2OUT:
    case MTI_2MOT:
      status.append(param.mid(1).constData());
      break;
    case MTI_1SHU: {
      quint8 st = param[1];
      quint8 percent = param[2];
      if (st == STOP) {
        status += "STOP ";
      } else if (st == UP) {
          status += "UP ";
      } else {
          status += "DOWN ";
      }
      status += QString::number(percent) + " %";
    }
      break;
    case MTI_8OUT:
    case MTI_8OUT3IN:
      status.append(param.mid(1).constData());
      break;
    case MTI_D4SHU:
      for (int i=1; i<9; ++i) {
        switch (param[i]) {
          case STOP: status += "ST"; break;
          case UP  : status += "UP"; break;
          case DOWN: status += "DN"; break;
        }
        status += QString::number(param[++i]) + " ";
      }
      break;
    case MTI_1DIM:
    case MTI_1DIM010V:
      status += "out " + QString::number(param[0]);
      switch (param[1]) {
        case 0: status += " STOP "; break;
        case 1: status += " UP "; break;
        case 2: status += " DOWN "; break;
      }
      status += QString::number(param[2]) + "%";
      break;
    case MTI_DIMRGB:
      switch (param[1]) {
        case 0: status += " VST"; break;
        case 1: status += " VUP"; break;
        case 2: status += " VDN"; break;
      }
      status += QString::number(param[2]) + "%";
      switch (param[3]) {
        case 0: status += " HST"; break;
        case 1: status += " HUP"; break;
        case 2: status += " HDN"; break;
      }
      val = static_cast<quint8>(param[4]) << 8;
      val |= static_cast<quint8>(param[5]);
      status += QString::number(val);
      switch (param[6]) {
        case 0: status += " SST"; break;
        case 1: status += " SUP"; break;
        case 2: status += " SDN"; break;
      }
      status += QString::number(param[7]) + "%";
      break;
    case MTI_TERMO: // It uses CLIMA instead
    case MTI_4IN: // No status
    case MTI_FTC2E: // No status
    default:
      status.append("-");
      break;
  }

  emit event(status);
}

unsigned char FtcInfo::getMessageType()
{
  return msg_type;
}

unsigned char FtcInfo::getCmd()
{
  return cmd;
}

int FtcInfo::getMti()
{
  return (int) mti;
}

int FtcInfo::getProductLot()
{
  return product_lot;
}

int FtcInfo::getSerial()
{
  return serial;
}

const QString FtcInfo::getAddress()
{
  return addr.toHex().toUpper();
}

const QByteArray FtcInfo::getMtiAddress()
{
  return addr;
}

const QByteArray FtcInfo::getParam()
{
  QByteArray ba = param;
  return ba;
}

bool FtcInfo::isCmd8Set()
{
  return f_cmd_bit8;
}

const QString FtcInfo::toDegree()
{
  QByteArray ba = param.mid(5, 2);
  QString degree = QString::number(ba[0]);
  quint8 value = static_cast<quint8>(ba[1]);

  switch (value) {
    case 0x20: degree += ".1"; break;
    case 0x40: degree += ".3"; break;
    case 0x60: degree += ".4"; break;
    case 0x80: degree += ".5"; break;
    case 0xA0: degree += ".6"; break;
    case 0xC0: degree += ".8"; break;
    case 0xE0: degree += ".9"; break;
    default: degree += ".0"; break;
  }

  return degree;
}

const QString FtcInfo::toTime()
{
  QByteArray ba = param;
  QString time = ba.mid(2, 2).toHex() + "-"
      + ba.mid(4, 1).toHex() + "-"
      + ba.mid(5, 1).toHex() + " " + ba.mid(0, 1).toHex() + ":"
      + ba.mid(1, 1).toHex();

  return time;
}

void FtcInfo::debugCommand()
{
  QString debug;
  QString mti_hex = QString("%1").arg(mti, 0, 16);

  debug.append(tr("mti ") + mti_hex + " address " + getAddress()
               + " command " + cmd);
  qDebug() << debug;
}

