#ifndef THERMODEVICE_H
#define THERMODEVICE_H

#include <QDialog>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QPushButton>

#include "thermodevicetype.h"

class ThermoDevice : public QDialog
{
  Q_OBJECT

public:

  explicit ThermoDevice(QWidget *parent = 0);
  ~ThermoDevice();


private slots:
  void setRadio1(bool);
  void setRadio2(bool);
  void setRadio3(bool);
  void sendResult();

private:
  QGroupBox *createSelectionGroup();
};

#endif // THERMODEVICE_H
