#include "ftc232.h"
#include <QDateTime>
#include <QDebug>

Ftc232::Ftc232(QObject *parent) :
  QObject(parent), port(NULL), portname(tr("COM1")), info(new FtcInfo()), f_writable(true)
{
  connect(info, SIGNAL(address(QString)), SIGNAL(sn(QString)));// trigger from signal address to active signal sn and then call the relation slot
  connect(info, SIGNAL(event(QString)), SIGNAL(message(QString)));
  connect(info, SIGNAL(temperature(QString)), SLOT(onFTCTemperature(QString)));
  connect(info, SIGNAL(time(QString)), SLOT(onFtcTime(QString)));
}

Ftc232::~Ftc232()
{
  if (port->isOpen())
    port->close();
  delete port;
  delete info;
}

QList<QString> Ftc232::discovery()
{
  QList<QString> portnames;
  QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

  for (int i = 0; i<ports.size(); i++){
    portnames.append(ports.at(i).portName);
  }

  return portnames;
}

FtcInfo &Ftc232::getFtcinfo()
{
  return *info;
}

void Ftc232::setPortname(const QString name)
{
  portname = name;
}

void Ftc232::open()
{
  PortSettings settings = {BAUD38400, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
  port = new QextSerialPort(portname, settings, QextSerialPort::EventDriven);

  if (!port->open(QIODevice::ReadWrite)) {
    emit error("Cannot open port");
    delete port;
    port = NULL;
    return;
  }
  connect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
  emit message(tr("FTC Start"));
}

void Ftc232::close()
{
  disconnect(port, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
  port->close();
  delete port;
  port = NULL;
  emit message(tr("FTC Stop"));
}

void Ftc232::onReadyRead()
{
  int count = port->bytesAvailable();

  while (count) {
    recv.append(port->readAll());
    count = port->bytesAvailable();
  }

  int end_index = recv.indexOf(ByteEnd);
  if (end_index > -1) {
    int start_index = recv.indexOf(ByteStart);

    if (start_index > -1) {
      QByteArray ftc = toFtcMessage(recv.mid(start_index, end_index - start_index));
      extractMessage(ftc);
    } else {
      emit message(tr("No Start Index"));
    }
    recv.clear();
  }
}

void Ftc232::onFtcTime(const QString msg)
{
  emit message(tr("Time ") + msg);
}

void Ftc232::onFTCTemperature(const QString msg)
{
  emit message(tr("Temperature ") + msg + " degree");
}

void Ftc232::extractMessage(const QByteArray &ba)
{
  if (!checksum(ba)) {
    qDebug() << "Check Sum Error" << ba.toHex().toUpper();
    return;
  }

  qDebug() << "\n" << QDateTime::currentDateTime().toString()
      << "Get" << ba.toHex().toUpper();
  info->setInfo(ba);
}

bool Ftc232::checksum(const QByteArray &ba)
{
  if (static_cast<quint8>(ba[ba.length() - 1]) == calChecksum(ba))
    return true;
  return false;
}

quint8 Ftc232::calChecksum(const QByteArray &ba)
{
  int size = static_cast<int>(ba[0]);
  quint8 sum = 0;

  for (int i = 0; i < size; ++i) {
    sum += ba[i];
  }
  sum ^= 0xff;

  return sum;
}

void Ftc232::writeFtcUart(const QByteArray &ftc_msg)
{
  QByteArray ba = fromFtcMessage(ftc_msg);
  qDebug() << "Set FTC" << ftc_msg.toHex().toUpper();
  if (f_writable)
    port->write(ba);
}

QByteArray Ftc232::toFtcMessage(const QByteArray &ba)
{
  QByteArray ftc;
  int size = ba.length();

  for (int i = 1; i < size; ++i) {
    if (ba[i] == ByteMask) {
      ftc.append(ba[++i] | 0x20);
    } else {
      ftc.append(ba[i]);
    }
  }

  return ftc;
}

QByteArray Ftc232::fromFtcMessage(const QByteArray &ba)
{
  QByteArray msg;
  int size = ba.length();

  msg.append(ByteStart);
  for (int i = 0; i < size; ++i) {
    if ((ba[i] == ByteStart) || (ba[i] == ByteMask)
        || (ba[i] == ByteEnd)) {
      msg.append(ByteMask);
      msg.append(ba[i] & ~(0x20));
    } else {
      msg.append(ba[i]);
    }
  }
  msg.append(ByteEnd);

  return msg;
}

void Ftc232::sendCommand(const unsigned char cmd, const QByteArray param)
{
  static quint8 msg_nr(0);
  QByteArray ba;

  ++msg_nr;
  switch (cmd) {
    case CMD_ADDRESS:
    case CMD_SOFT_VERSION:
      ba.append(SRC_AUTOMATION + DST_AUTOMATION + ENQUIRY);
      ba.append(cmd);
      ba.append(MTI_ALL);
      ba.append(msg_nr);
      ba.insert(0, ba.length() + 1); // data length + itself
      ba.append(calChecksum(ba));
      writeFtcUart(ba);
      break;
    case CMD_STATUS:
      if (info->getMtiAddress().length() != 4)
        return;
      ba.append(SRC_AUTOMATION + DST_AUTOMATION + ENQUIRY);
      ba.append(cmd);
      ba.append(info->getMtiAddress());
      ba.append(0xff);  // All outputs
      ba.append(msg_nr);
      ba.insert(0, ba.length() + 1); // data length + itself
      ba.append(calChecksum(ba));
      writeFtcUart(ba);
      break;
    case CMD_FACTORY_RESET:
      ba.append(SRC_AUTOMATION + DST_AUTOMATION + REQUEST);
      ba.append(cmd);
      ba.append(info->getMtiAddress());
      ba.append(msg_nr);
      ba.insert(0, ba.length() + 1); // data length + itself
      ba.append(calChecksum(ba));
      writeFtcUart(ba);
      break;
    case CMD_TOGGLE:
      ba.append(SRC_AUTOMATION + DST_AUTOMATION + REQUEST);
      ba.append(cmd);
      ba.append(info->getMtiAddress());
      ba.append(param[0]);
      ba.append(msg_nr);
      ba.insert(0, ba.length() + 1); // data length + itself
      ba.append(calChecksum(ba));
      writeFtcUart(ba);
      break;
    case CMD_ANALOG:
    case CMD_MODE:
    case CMD_RESET:
    case CMD_CONFIG:
    case CMD_TEST:
    default:
      ba.append(SRC_AUTOMATION + DST_AUTOMATION + REQUEST);
      ba.append(cmd);
      ba.append(info->getMtiAddress());
      ba.append(param);
      ba.append(msg_nr);
      ba.insert(0, ba.length() + 1); // data length + itself
      ba.append(calChecksum(ba));
      writeFtcUart(ba);
      break;
  }
}

void Ftc232::sendFactoryReset()
{
  sendCommand(CMD_FACTORY_RESET);
}

void Ftc232::sendSoftwareVersion()
{
  sendCommand(CMD_SOFT_VERSION);
}

void Ftc232::sendStatus()
{
  sendCommand(CMD_STATUS);
}

void Ftc232::sendToggle(const int out)
{
  QByteArray param;

  param.append(out & 0xFF);
  sendCommand(CMD_TOGGLE, param);
}

void Ftc232::sendSetOutput(const int number)
{
  QByteArray ba;
  int mti = info->getMti();

  if ((mti == MTI_8OUT)
      || (mti == MTI_8OUT3IN)
      ) {
    ba.append(number);
    ba.append(100);
    sendCommand(CMD_ANALOG, ba);
  } else {
    ba.append(number);
    sendCommand(CMD_RESET, ba);
  }
}

void Ftc232::sendResetOutput(const int number)
{
  QByteArray ba;
  int mti = info->getMti();

  if ((mti == MTI_8OUT)
      || (mti == MTI_8OUT3IN)
      ) {
    ba.append(number);
    ba.append('\0');
    sendCommand(CMD_ANALOG, ba);
  } else {
    ba.append(number);
    sendCommand(CMD_RESET, ba);
  }
}

void Ftc232::sendMode(const int mode)
{
  QByteArray ba;
  ba.append(0x01);
  ba.append(mode);
  sendCommand(CMD_MODE, ba);
}

void Ftc232::sendConfigWthermo()
{
  QByteArray ba;
  ba.append(CNF_RELAY_MODE);
  ba.append(UNIT_AIRFLOW2);
  sendCommand(CMD_CONFIG, ba);
}

void Ftc232::sendConfigDthermoMode()
{
  QByteArray ba;
  ba.append(CNF_RELAY_MODE);
  ba.append(UNIT_AIRFLOW4);
  sendCommand(CMD_CONFIG, ba);
}

void Ftc232::sendConfigDthermoSummer(bool on)
{
  QByteArray ba;
  ba.append(CNF_CYCLE);
  ba.append(on);
  sendCommand(CMD_CONFIG, ba);
}

void Ftc232::sendTest(const int out)
{
  QByteArray ba;
  ba.append(out);
  sendCommand(CMD_TEST, ba);
}

void Ftc232::sendAnalogRGB(const int out, const int dimm, const int hue_h,
    const int hue_l, const int sat)
{
  QByteArray ba;
  ba.append(out);   // For D-DIMRGB/W-1DIMRGB, it has to be 1
  ba.append(dimm);  // Dim value level (0 - 100%)
  ba.append(hue_h); // High byte of hue degree (0 - 360)
  ba.append(hue_l); // Low byte of hue degree
  ba.append(sat);   // Saturation (0 - 100%)
  sendCommand(CMD_ANALOG, ba);
}

