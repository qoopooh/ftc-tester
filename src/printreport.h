#ifndef PRINTREPORT_H
#define PRINTREPORT_H

#include <QDialog>
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QBoxLayout>
#include <QPushButton>
#include <QCalendarWidget>
#include <QRadioButton>

#include "MTI.h"

class PrintReport : public QDialog
{
  Q_OBJECT
public:
  explicit PrintReport(QWidget *parent = 0);
  ~PrintReport();

signals:
  void report(QString start, QString finish, QString mti, QString header, char);

public slots:
  void onExport();

private slots:
  void onCancel();

private:
  void createGroupBoxDate();
  void createGroupBoxProduct();
  void createMtiList();

  QGroupBox *groupBoxDate;
  QGroupBox *groupBoxProduct;

  QLabel *labelStartDate;
  QLabel *labelFinishDate;
  QLabel *labelMti;
  QComboBox *comboBoxMti;
  QPushButton *pushButtonExport;
  QPushButton *pushButtonCancel;
  QCalendarWidget *calendarStart;
  QCalendarWidget *calendarFinish;
  QRadioButton *radioButtonAll;
  QRadioButton *radioButtonPass;
  QRadioButton *radioButtonFail;
  QRadioButton *radioButtonLast;
};

#endif // PRINTREPORT_H
