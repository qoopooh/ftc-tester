#-------------------------------------------------
#
# Project created by Berm 2013-05-31T17:00:00
#
#-------------------------------------------------

QT       += core gui sql
include(lib/qextserialport-1.2rc/src/qextserialport.pri)
CONFIG += qextserialport
lessThan(QT_MAJOR_VERSION, 5) {
} else {
  QT += widgets
}
TARGET = DomoticsFtcTester-V1.3
TEMPLATE = app

HEADERS  += mainwindow.h \
    log.h \
    ftc232.h \
    MTI.h \
    ftc_commands.h \
    database.h \
    tester.h \
    ftcinfo.h \
    printreport.h \
    thermodevice.h \
    dimrgbdevice.h \
    thermodevicetype.h \
    dimrgbdevicetype.h

SOURCES += main.cpp\
    mainwindow.cpp \
    log.cpp \
    ftc232.cpp \
    database.cpp \
    tester.cpp \
    ftcinfo.cpp \
    printreport.cpp \
    thermodevice.cpp \
    dimrgbdevice.cpp \
    thermodevicetype.cpp \
    dimrgbdevicetype.cpp

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc

win32 {
    RC_FILE = dt.rc # icon on exe file
}

