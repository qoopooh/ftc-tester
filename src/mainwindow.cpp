#include "mainwindow.h"
#include "ui_mainwindow.h"

const QString kVersion("V1.3");
const QString kProcessingColor("QLabel { background-color: wheat; }");
const QString kPassColor("QLabel { background-color: green; }");
const QString kFailColor("QLabel { background-color: red; }");
const QString kDisableColor("QLabel { background-color: gray; }");
const QString kPassedUnit(QString::fromUtf8("Duplicated serial number"));
const QString kPassedUnitTh(QString::fromUtf8("หมายเลขอุปกรณ์ซ้ำ"));
const QString kRetestTh(QString::fromUtf8("ต้องการทดสอบอุปกรณ์อีกครั้ง"));
const QString kDatabaseError("Database Error!");
const QString kRs232Error("FTC Error!");
const QString kReservedLot(QString::fromUtf8("This serial number is invalid\nPlease contact Engineering to solve the serialization"));
const QString kReservedLotTh(QString::fromUtf8("หมายเลขนี้ไม่อนุญาตให้ใช้งาน\nกรุณาติดต่อฝ่ายวิศวกรรมเพื่อทำการปรับปรุงหมายเลข"));

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow), ftc(new Ftc232(this)),
  db(new Database(this)), test(new Tester(this)),
  printdialog(new PrintReport(this)),
  thermodialog(new ThermoDevice(this)),
  dimrgbdialog(new DimRgbDevice(this)),
  f_rf_check(false), f_rf_rc_4brf(false)
{
  ui->setupUi(this);
  setWindowTitle(windowTitle() + kVersion);

  connect(ftc, SIGNAL(sn(QString)), SLOT(onSerialNumber(QString)));
  connect(ftc, SIGNAL(message(QString)), ui->labelMessage, SLOT(setText(QString)));
  connect(ftc, SIGNAL(ftcStatus(QString)), ui->statusBar, SLOT(showMessage(QString)));
  connect(ftc, SIGNAL(error(QString)), SLOT(onFtcError(QString)));
  connect(ui->comboBoxPort, SIGNAL(currentIndexChanged(QString)), ftc, SLOT(setPortname(QString)));
  connect(db, SIGNAL(error(int, QString)), SLOT(onDatabaseError(int, QString)));
  connect(test, SIGNAL(result(quint16)), SLOT(onTestResult(quint16)));
  connect(test, SIGNAL(requestStatus()), ftc, SLOT(sendStatus()));
  connect(test, SIGNAL(requestSoftwareVersion()), ftc, SLOT(sendSoftwareVersion()));
  connect(test, SIGNAL(requestFactoryReset()), ftc, SLOT(sendFactoryReset()));
  connect(test, SIGNAL(requestToggle(int)), ftc, SLOT(sendToggle(int)));
  connect(test, SIGNAL(requestSetOutput(int)), ftc, SLOT(sendSetOutput(int)));
  connect(test, SIGNAL(requestResetOutput(int)), ftc, SLOT(sendResetOutput(int)));
  connect(test, SIGNAL(requestMode(int)), ftc, SLOT(sendMode(int)));
  connect(test, SIGNAL(requestConfigWthermo()), ftc, SLOT(sendConfigWthermo()));
  connect(test, SIGNAL(requestConfigDthermoMode()), ftc, SLOT(sendConfigDthermoMode()));
  connect(test, SIGNAL(requestConfigDthermoSummer(bool)), ftc, SLOT(sendConfigDthermoSummer(bool)));
  connect(test, SIGNAL(requestTest(int)), ftc, SLOT(sendTest(int)));
  connect(test, SIGNAL(requestRGB(int,int,int,int,int)), ftc, SLOT(sendAnalogRGB(int,int,int,int,int)));
  connect(test, SIGNAL(requestVersion()), this, SLOT(on_pushButtonVersion_clicked()));
  connect(test, SIGNAL(message(QString)), ui->labelMessage, SLOT(setText(QString)));
  connect(printdialog, SIGNAL(report(QString,QString,QString,QString,char)), SLOT(onReport(QString,QString,QString,QString,char)));
  connect(thermodialog, SIGNAL(finished(int)), this, SLOT(onThermoDialog(int)));//finish ->return done(int)
  connect(dimrgbdialog, SIGNAL(finished(int)), this, SLOT(onDimrgbDialog(int)));//finish ->return done(int)
  searchFtc232();

  ui->labelSerial->setStyleSheet(kDisableColor);
  ui->labelMessage->setStyleSheet(kDisableColor);
  ui->statusBar->showMessage(tr("Started!"));

  ui->checkBoxConnect->setChecked(true);
  showMaximized();
}

MainWindow::~MainWindow()
{
  delete ui;
  delete thermodialog;
  delete dimrgbdialog;
  delete ftc;
  delete db;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
  int w = event->size().width();
  int h = event->size().height();

  ui->groupBoxControl->resize(w - 20, ui->groupBoxControl->size().height());
  ui->groupBoxMonitor->resize(w - 20, h - 145);
  QSize gms = ui->groupBoxMonitor->size();
  ui->labelSerial->resize(gms.width() - 10, gms.height() * 0.6);
  QFont font = ui->labelSerial->font();
  font.setPixelSize(ui->labelSerial->height() / 2.5);
  ui->labelSerial->setFont(font);
  ui->labelMessage->setGeometry(QRect(5, ui->labelSerial->height() + 20,
                                      gms.width() - 10,
                                      gms.height() - ui->labelSerial->height() - 25));
  font = ui->labelMessage->font();
  font.setPixelSize((ui->labelMessage->height() / 4) + 10);
  ui->labelMessage->setFont(font);
  ui->pushButtonFull->setGeometry(
        QRect(w - ui->pushButtonFull->width() - 20,
              ui->pushButtonFull->y(),
              ui->pushButtonFull->width(),
              ui->pushButtonFull->height()));
  ui->pushButtonReport->setGeometry(
        QRect(ui->groupBoxControl->width() - ui->pushButtonReport->width() - 8,
             ui->groupBoxControl->height() - ui->pushButtonReport->height() - 8,
              ui->pushButtonReport->width(),
              ui->pushButtonReport->height()));
}

void MainWindow::onTimeout()
{
  static unsigned long count = 0;

  ++ count;

  if ((count % 50) == 0) {
  }
}

void MainWindow::onSerialNumber(const QString sn)
{
  static bool f_retest(false);
  static bool f_tested(false);
  static bool f_passed(false);
  static bool f_committed(false);
  int result;
  QString previous_sn = ui->labelSerial->text();
  QString software_version;
  bool f_virtual(false);
  FtcInfo *ftcinfo = &ftc->getFtcinfo();

  // D-BUSSUPIP several MTI number
  if (test->isVirtualButton(previous_sn, sn))
      f_virtual = true;
  if (!f_virtual && (sn.length() == 8))
    ui->labelSerial->setText(sn);

  // Sergio's production lot
  if (isReservedProductLot(ftcinfo->getProductLot()))
    return;

  // Several unit with the same MTI (Popup to select device)
  if (isFirstThermoDevice(ftcinfo->getMti())) {
    stopTest();
    thermodialog->setWindowModality(Qt::WindowModal);
    thermodialog->show();
    return;
  } else if (isFirstDimrgbDevice(ftcinfo->getMti())) {
    stopTest();
    dimrgbdialog->setWindowModality(Qt::WindowModal);
    dimrgbdialog->show();
    return;
  }

  // Decision table, difficult to change!
  if (f_virtual || previous_sn.contains(sn)) {
    if (f_passed) {
      test->exec(*ftcinfo);
      return;
    }
    if (f_tested) {
      if (!f_retest) {
        return;
      }
    }
    test->exec(*ftcinfo);

    // Commit test result if finish
    if (test->isCompleted() && !f_committed) {
      result = test->getResult();
      software_version = test->getSofwareVersion();
      if (db->commitResult(previous_sn, software_version, result, thermo_type,dimrgb_type)) {
        ui->labelSerial->setStyleSheet(kPassColor);
        f_committed = true;
      }
    }
  } else if ((ftcinfo->getMti() == MTI_H3IN)
             && (ftcinfo->getProductLot() == 0)) {
    // Dummy remote to test W-2VLESS
    qDebug() << "Engineering Unit";
    test->exec(*ftcinfo);
  } else { // Diff unit
    db->addCommand(sn);
    if (test->isRunning() && !f_passed && !(f_tested && !f_retest)) {
      int previous_result = test->getResult();
      software_version = test->getSofwareVersion();
      if (previous_result
          && db->commitResult(previous_sn, software_version, previous_result,
                              thermo_type,dimrgb_type)) {
        ui->statusBar->showMessage("Commit unfinished result");
      }
    }
    f_tested = false;
    f_retest = false;
    f_passed= false;
    f_committed = false;
    if (db->isTestedSerial(sn, &result)) {
      f_tested = true;
      stopTest();
      ui->labelSerial->setStyleSheet(kFailColor);

      // Check passed unit
      if (test->isPassTest(sn, result)) {
        test->setPassedUnit(sn);
        f_passed = true;
        QMessageBox::critical(this, "Passed Unit", kPassedUnit
                              + "\n" + kPassedUnitTh);
        ui->statusBar->showMessage(kPassedUnit + " " + kPassedUnitTh);
        startTest();
        return;
      }

      // Check re-test unit
      switch(QMessageBox::question(
        this,
        tr("Duplicated Test"),
        tr("Do you want to re-test ") + sn + "?\n" + kRetestTh,
        QMessageBox::Yes |
        QMessageBox::No) )
      {
        case QMessageBox::Yes:
          f_retest = true;
          ui->labelSerial->setStyleSheet(kProcessingColor);
          test->initTest(sn, *ftcinfo);
          test->exec(*ftcinfo);
          break;
        case QMessageBox::No:
        default:
          f_retest = false;
          ui->labelSerial->setStyleSheet(kDisableColor);
          break;
      }

      startTest();
    } else {
      test->exec(*ftcinfo);
      ui->labelSerial->setStyleSheet(kProcessingColor);
      f_tested = false;
      ui->statusBar->showMessage("Testing");
    }
  }
}

void MainWindow::onTestResult(const quint16 res)
{
  ui->statusBar->showMessage(tr("Test result: ") + QString::number(res, 16));
}

void MainWindow::onDatabaseError(const int err, const QString msg)
{
  stopTest();
  switch (err) {
    case Database::CannotOpen:
      QMessageBox::critical(this, kDatabaseError, msg 
            + "\n" + "Please try to check internet connection");
      close();
      break;
    case Database::CannotSelect:
    case Database::CannotInsert:
    default:
      QMessageBox::information(this, kDatabaseError, msg);
      break;
  }
  startTest();
}

void MainWindow::onFtcError(const QString msg)
{
  QMessageBox::information(this, kRs232Error, msg);
  ui->labelMessage->setText(msg);
  ui->checkBoxConnect->toggle();
}

void MainWindow::onReport(const QString start, const QString finish,
                          const QString mti, const QString header, const char f)
{
  QString data(db->createReport(start, finish, mti, f));//convert database file 
  // to string datas
  QTextStream in(&data);
  QString filename(header + ".html");
  QFile file(filename);
  QFileInfo fileinfo(file);
  if ( file.open(QIODevice::WriteOnly) ) {
    int col_number=4;
    if((mti=="")||(mti==QString::number(MTI_TERMO, 16).toUpper())||(mti==QString::number(MTI_DIMRGB, 16).toUpper())){
        col_number=5;
    }
    QTextStream stream( &file );
    stream << "<!DOCTYPE html>" << endl;
    stream << "<html>" << endl;
    stream << "<head>" << endl;
    stream << "</head>" << endl;
    stream << "<body>" << endl;
    stream << "<table border='1'><caption>" << header
           << " (" << windowTitle() << " on "
           << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm")
           << ")</caption>" << endl;

   if(col_number==5){
     stream << "<tr><th>No.</th>\n\t<th>Serial Number</th>\n\t<th>"
               << "Firmware Version</th>\n\t<th>Result</th>\n\t<th>"
               << "Tested Date</th>\n\t<th>Product Name</th></tr>" << endl;
    }else{
     stream << "<tr><th>No.</th>\n\t<th>Serial Number</th>\n\t<th>"
               << "Firmware Version</th>\n\t<th>Result</th>\n\t<th>"
               << "Tested Date</th></tr>" << endl;
    }


    while (!in.atEnd()) {
      QStringList line = in.readLine().split('\t');
      //test->setThermoType(static_cast<ThermoDevice::UnitType>(i));
      QString product_type(line.at(5).toStdString().c_str());
      if (QString::compare(product_type, thermo_type.k_wthermo) == 0) {
        test->setThermoType(ThermoDeviceType::Wthermo);
      } else if (QString::compare(product_type, thermo_type.k_dthermo) == 0) {
        test->setThermoType(ThermoDeviceType::Dthermo);
      } else if (QString::compare(product_type, thermo_type.k_w1irth) == 0) {
        test->setThermoType(ThermoDeviceType::W1irth);
      } else if (QString::compare(product_type, dimrgb_type.k_w1Dimrgb) == 0){
        test->setDimrgbType(DimRgbDeviceType::W1dimrgb);
      } else if (QString::compare(product_type, dimrgb_type.k_dDimrgb) == 0){
        test->setDimrgbType(DimRgbDeviceType::Ddimrgb);
      }

      //test->setThermoType(static_cast<ThermoDevice::UnitType>());
      if (f == 'p') {
        if (!test->isPassTest(line.at(1), line.at(3).toInt()))//compare result
        // with with testcase :(MTI,Result)
          continue;
      } else if (f == 'f') {
        if (test->isPassTest(line.at(1), line.at(3).toInt()))//compare result
        // with with testcase :(MTI,Result)
          continue;
      } else {
      }
      stream << "<tr>";
      for (int i = 0; i < line.length(); ++i) {
        stream << endl << "\t<td>" << line[i] << "</td>";
      }
      stream << "</tr>" << endl;
    }
    stream << "</table>" << endl;
    stream << "</body>" << endl;
    stream << "</html>" << endl;
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileinfo.absoluteFilePath()));

    test->setThermoType(thermo_type.getType()); //load previous tester type
    test->setDimrgbType(dimrgb_type.getType()); //load previous tester type

  }
}

void MainWindow::onThermoDialog(int i)
{
  startTest();
  if (i == 0)
    return;
  thermo_type.setType(static_cast<ThermoDeviceType::UnitType>(i));
  test->setThermoType(thermo_type.getType());
  qDebug() << "onThermoDialog" << i;
  on_pushButtonClear_clicked();
  on_pushButtonVersion_clicked();
}

void MainWindow::onDimrgbDialog(int i)
{
  startTest();
  if (i == DimRgbDeviceType::None)
    return;
  dimrgb_type.setType(static_cast<DimRgbDeviceType::UnitType>(i));//set for report
  test->setDimrgbType(dimrgb_type.getType());//set for tester
  on_pushButtonClear_clicked();
  on_pushButtonVersion_clicked();
}

void MainWindow::on_pushButtonRefresh_clicked()
{
  searchFtc232();
}

void MainWindow::on_checkBoxConnect_toggled(bool checked)
{
  if (checked) {
    ftc->open();
  } else {
    test->strengthClear();
    ftc->close();
  }
  setGroupDisabled(checked);
}

void MainWindow::on_pushButtonAddress_clicked()
{
  ftc->sendCommand(CMD_ADDRESS);
}

void MainWindow::on_pushButtonClear_clicked()
{
  ui->labelSerial->setText("-");
  ui->labelMessage->setText("-");
  ui->labelMessage->setStyleSheet(kDisableColor);
  test->clearUnit();
}

void MainWindow::on_pushButtonFactory_clicked()
{
  ftc->sendFactoryReset();
}

void MainWindow::on_pushButtonVersion_clicked()
{
  ftc->sendSoftwareVersion();
}

void MainWindow::on_pushButtonStatus_clicked()
{
  ftc->sendStatus();
}

void MainWindow::on_pushButtonFull_clicked()
{
  if (isFullScreen()) {
    showNormal();
    ui->pushButtonFull->setText("Full Screen");
  } else {
    showFullScreen();
    ui->pushButtonFull->setText("Exit");
  }
}

void MainWindow::on_pushButtonReport_clicked()
{
  printdialog->show();
}

void MainWindow::on_pushButtonStrength_clicked()
{
  test->strengthToggle();
}

void MainWindow::setGroupDisabled(bool checked)
{
  ui->groupBoxConnection->setDisabled(checked);
  ui->groupBoxControl->setEnabled(checked);
  ui->groupBoxMonitor->setEnabled(checked);
}

void MainWindow::searchFtc232()
{
  QList<QString> channels = Ftc232::discovery();

  ui->comboBoxPort->clear();
  for (int i = 0; i<channels.size(); i++){
    ui->comboBoxPort->addItem(channels.at(i));
  }
  ui->comboBoxPort->setCurrentIndex(ui->comboBoxPort->count() - 1);
}

bool MainWindow::isReservedProductLot(int lot)
{
  if (lot != 0x10) // This lot number is reserved for Sergio
    return false;

  stopTest();
  ui->labelSerial->setStyleSheet(kFailColor);
  QMessageBox::critical(this, "Reserved Product Lot Number!", kReservedLot
                        + "\n" + kReservedLotTh);
  startTest();
  return true;
}

bool MainWindow::isFirstThermoDevice(int mti)
{
  if ((mti != MTI_TERMO) || (thermo_type.getType() != ThermoDeviceType::None))
    return false;
  return true;
}

bool MainWindow::isFirstDimrgbDevice(int mti)
{
  if ((mti != MTI_DIMRGB) || (dimrgb_type.getType() != DimRgbDeviceType::None))
    return false;
  return true;
}

void MainWindow::stopTest()
{
  disconnect(ftc, SIGNAL(sn(QString)), this, SLOT(onSerialNumber(QString))); //disconnect ftc object signal from main object with onSerialNumber slot
}

void MainWindow::startTest()
{
  connect(ftc, SIGNAL(sn(QString)), SLOT(onSerialNumber(QString))); //connect ftc object signal to main object with onSerialNumber slot
}


