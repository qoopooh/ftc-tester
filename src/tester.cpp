#include "tester.h"
#include "ftcinfo.h"

//DimRgbDevice::UnitType dimrgb_type;

Tester::Tester(QObject *parent) :
  QObject(parent), state(Init), timer(new QTimer(this)), res(0),
  f_strength(false)
{
  timer->setInterval(125);
  timer->start();
  connect(timer, SIGNAL(timeout()), SLOT(onTimeout()));
}

Tester::~Tester()
{
  delete timer;
}

void Tester::initTest(const QString &sn, FtcInfo &localinfo)
{
  qDebug() << "initTest";
  unitname = sn;
  mti = unitname.mid(0, 2).toInt(NULL, 16);
  setState(Init);
  res = 0;
  f_device_pending = false;
  version = "";

  if (localinfo.getMti() != MTI_H3IN) // Do not ask firmware version from RC-4BRF
    emit requestSoftwareVersion();
}

void Tester::onTimeout()
{
  static int count = 0;
  static int relaystate = 0;
  static int out8 = 0;

  if (!f_strength)
    return;
  ++count;
  if ((count & 0x0003) == 0) {

    if (mti == MTI_8OUT
        || mti == MTI_8OUT3IN) {
      switch (relaystate) {
        case 0:
          emit requestSetOutput(++out8);
          if (out8 >= 8) {
            out8 = 0;
            relaystate = 2;
          } else {
            relaystate = 1;
          }
          break;
        case 1:
          relaystate = 0;
          break;
        case 2:
          emit requestResetOutput(++out8);
          if (out8 >= 8) {
            out8 = 0;
            relaystate = 0;
          } else {
            relaystate = 3;
          }
          break;
        case 3:
          relaystate = 2;
          break;
        default:
          relaystate = 0;
          break;
      }
    } else if (mti == MTI_2OUT
               || mti == MTI_2MOT) {
      switch (relaystate) {
        case 0:
          relaystate = 1;
          emit requestToggle(1);
          break;
        case 1:
          relaystate = 0;
          emit requestToggle(3);
          break;
        default:
          relaystate = 0;
          break;
      }
    }
  }
}

bool Tester::isRunning()
{
  return (state == Run);
}

bool Tester::isCompleted()
{
  return (state == Finish);
}

/**
  * Check D-BUSSUPIP virtual button
  */
bool Tester::isVirtualButton(QString o_bussupip, QString n_bussupip)
{
  bool ok(false);

  if (o_bussupip.length() != 8)
    return false;
  QString mti_str = o_bussupip.left(2);
  int o_val = mti_str.toInt(&ok, 16);
  if (!ok)
    return false;

  if (o_val != MTI_FTC2E) {
    return false;
  }
  unsigned char cmd = ftcinfo->getCmd();
  if (!((cmd == CMD_TOGGLE) || (cmd == CMD_SET) || (cmd == CMD_RESET)))
    return false;
  o_val = o_bussupip.toInt(&ok, 16);
  if (!ok)
    return false;
  int n_val = n_bussupip.toInt(&ok, 16);
  if (!ok)
    return false;
  return o_val + 1 == n_val;
}

bool Tester::isPassTest(const QString &sn, const int res)
{
  unsigned char _mti = sn.mid(0, 2).toInt(NULL, 16);

  return isFinished(_mti, res);
}

quint16 Tester::getResult()
{
  return res;
}

QString Tester::getSofwareVersion()
{
  return version;
}

void Tester::exec(const FtcInfo &info)
{
  ftcinfo = const_cast<FtcInfo *>(&info);
  QString sn(ftcinfo->getAddress());

  if (!unitname.contains(sn)) {
    if (!((ftcinfo->getMti() == MTI_H3IN) && (ftcinfo->getProductLot() == 0))
        && !isVirtualButton(unitname, sn))
      initTest(sn, *ftcinfo);
  }

  switch (mti) {
    case MTI_2OUT:
      testW2onoff();
      break;
    case MTI_1SHU:
      testW1shutter();
      break;
    case MTI_8OUT:
    case MTI_8OUT3IN:
      testD8onoff();
      break;
    case MTI_D4SHU:
      testD4shutter();
      break;
    case MTI_TERMO:
      testWthermo();  // W-THERMO, D-THERMO and W-1IRTH
      break;
    case MTI_4IN:
      testW4in();
      break;
    case MTI_1DIM:
    case MTI_1DIM010V:
      testW1dim();
      break;
    case MTI_DIMRGB:
      if (dimrgb_type.getType()== DimRgbDeviceType::W1dimrgb)
        testW1dimrgb();
      else
        testDdimrgb();
      break;
    case MTI_FTC2E:
      testDgateway();
      break;
    case MTI_2MOT:
      testW2vless();
      break;
    case MTI_H3IN:
      if (ftcinfo->getProductLot() == 0) {
        // Dummy remote to test W-2VLESS
        qDebug() << "Dummy remote to test W-2VLESS" << res;
        testW2vless();
      } else {
        qDebug() << "RC test";
        testRc4brf();
      }
      break;
    default:
      break;
  }
}

void Tester::setPassedUnit(const QString &sn)
{
  unitname = sn;
  mti = unitname.mid(0, 2).toInt(NULL, 16);
  setState(Finish);
  res = 0;
  f_device_pending = false;
  version = "-";
}

void Tester::clearUnit()
{
  unitname = "00000000";
}

void Tester::strengthToggle()
{
  f_strength ^= 1;
}

void Tester::strengthClear()
{
  f_strength = false;
}

void Tester::setThermoType(ThermoDeviceType::UnitType type)
{
  thermo_type.setType(type);
}

void Tester::setDimrgbType(DimRgbDeviceType::UnitType type)
{
  dimrgb_type.setType(type);
}

bool Tester::setState(TestState st)
{
  if ((st == Run) && (state != Init)) {
    return false;
  }
  if ((st == Finish) && (state != Finish)) {
    emit requestStatus();
  }
  state = st;

  return true;
}

Tester::MoveState Tester::toMoveState(quint8 dat)
{
  MoveState state;

  if (dat == 0) {
    state = Stop;
  } else if (dat == 1) {
    state = Up;
  } else if (dat == 2) {
    state = Down;
  } else {
    state = Unknown;
  }


  return state;
}

bool Tester::isFinished(const unsigned char mti, const quint16 res)
{
  bool fin(false);

  switch (mti) {
    case MTI_2OUT:
    case MTI_1SHU:
    case MTI_1DIM:
    case MTI_1DIM010V:
      if (res == W2onoffPass)
        fin = true;
      break;
    case MTI_DIMRGB:
      if (dimrgb_type.getType() == DimRgbDeviceType::W1dimrgb) {
        if (res == W1dimrgbPass)
          fin = true;
      } else {
        if (res == DdimrgbPass)
          fin = true;
      }
      break;
    case MTI_8OUT:
    case MTI_8OUT3IN:
    case MTI_D4SHU:
      if (res == D8onoffPass)
        fin = true;
      break;
    case MTI_TERMO:
      if (thermo_type.getType() == ThermoDeviceType::W1irth) {
        if (res == W1irthPass)
          fin = true;
      } else {
        if (res == WthermoPass)
          fin = true;
      }
      break;
    case MTI_4IN:
      if (res == W4inPass)
        fin = true;
      break;
    case MTI_FTC2E:
      if (res == DgatewayPass)
        fin = true;
      break;
    case MTI_2MOT:
      if (res == W2vlessPass)
        fin = true;
      break;
    case MTI_H3IN:
      if (res == Rc4brfPass)
        fin = true;
      break;
    default:
      break;
  }

  return fin;
}

void Tester::testW2onoff()
{
  static QByteArray previous_output_status;
  unsigned char cmd = ftcinfo->getCmd();
  QByteArray output_status = ftcinfo->getParam().mid(1, 2);

  switch (cmd) {
    case CMD_STATUS:
      if (previous_output_status.length() == 2) {
        if (output_status[0] != previous_output_status[0]) {
          if (f_device_pending) {
            f_device_pending = false;
            res |= W2onoffA;
          }
        }
        if (output_status[1] != previous_output_status[1]) {
          if (f_device_pending) {
            f_device_pending = false;
            res |= W2onoffB;
          }
        }
      }
      if (state == Finish) {
        if ((output_status.at(0) == 'S') || (output_status.at(1) == 'S')) {
          emit requestFactoryReset();
        }
      }
      previous_output_status = output_status;
      break;
    case CMD_TOGGLE:
      res |= W2onoffTouch;
      emit requestStatus();
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0)
        res |= W2onoffProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testW1shutter()
{
  unsigned char cmd = ftcinfo->getCmd();
  quint8 status(0xff);

  switch (cmd) {
    case CMD_STATUS:
      status = ftcinfo->getParam().mid(1, 1)[0];
      if (f_device_pending) {
        f_device_pending = false;
        if (status == DOWN) {
          res |= W2onoffA;
        }
        if (status == UP) {
          res |= W2onoffB;
        }
      }
      if (state == Finish) {
        if ((status == UP) || (status == DOWN)) {
          emit requestToggle(1);
        } else if (status == STOP) {
          emit requestFactoryReset();
        }
      }
      break;
    case CMD_TOGGLE:
      res |= W2onoffTouch;
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0)
        res |= W2onoffProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    case CMD_SET:
    case CMD_RESET:
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testD8onoff()
{
  unsigned char cmd = ftcinfo->getCmd();
  static QByteArray previous_output_status;
  QByteArray output_status = ftcinfo->getParam().mid(1);

  switch (cmd) {
    case CMD_STATUS:
      if (state == Finish) {
        if ((output_status.contains('S'))) {
          emit requestFactoryReset();
        }
      }
      previous_output_status = output_status;
      break;
    case CMD_TOGGLE:
      if (ftcinfo->isCmd8Set()) {
        int i = previous_output_status.indexOf('S');
        switch (ftcinfo->getParam()[0]) {
          case 1:
            res |= D8onoffIn1;
            if (i == 0)
              res |= D8onoff1;
            else if (i == 1)
              res |= D8onoff2;
            else if (i == 2)
              res |= D8onoff3;
            else if (i == 3)
              res |= D8onoff4;
            break;
          case 2:
            res |= D8onoffIn2;
            if (i == 4)
              res |= D8onoff5;
            else if (i == 5)
              res |= D8onoff6;
            break;
          case 3:
            res |= D8onoffIn3;
            if (i == 6)
              res |= D8onoff7;
            else if (i == 7)
              res |= D8onoff8;
            break;
        }
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0)
        res |= D8onoffProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init) {
          previous_output_status.clear();
          setState(Run);
        }
      }
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testD4shutter()
{
  unsigned char cmd = ftcinfo->getCmd();
  QByteArray output_status = ftcinfo->getParam();
  static QByteArray previous_output_status;
  quint8 status(0);

  switch (cmd) {
    case CMD_STATUS:
      if (state == Finish)
        emit requestFactoryReset();
      previous_output_status = output_status;
      break;
    case CMD_TOGGLE:
      if (ftcinfo->isCmd8Set()) {
        switch (ftcinfo->getParam()[0]) {
          case 1:
            res |= D8onoffIn1;
            status = previous_output_status.at(1);
            if (status == DOWN) {
              res |= D8onoff1;
              break;
            } else if (status == UP) {
              res |= D8onoff2;
              break;
            }
            status = previous_output_status.at(3);
            if (status == DOWN) {
              res |= D8onoff3;
            } else if (status == UP) {
              res |= D8onoff4;
            }
            break;
          case 2:
            res |= D8onoffIn2;
            status = previous_output_status.at(5);
            if (status == DOWN) {
              res |= D8onoff5;
            } else if (status == UP) {
              res |= D8onoff6;
            }
            break;
          case 3:
            res |= D8onoffIn3;
            status = previous_output_status.at(7);
            if (status == DOWN) {
              res |= D8onoff7;
            } else if (status == UP) {
              res |= D8onoff8;
            }
            break;
        }
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0)
        res |= D8onoffProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init) {
          setState(Run);
        }
      }
      break;
    case CMD_FACTORY_RESET:
      if (state == Run) {
        res = 0;
      }
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

/**
 * @brief Tester::testWthermo Test thermo device group
 */
void Tester::testWthermo()
{
  unsigned char cmd(ftcinfo->getCmd());
  static unsigned char previous_cmd(cmd);
  static int device_cmd_count(0);

  switch (cmd) {
    case CMD_CLIMA:
      if ((state == Finish) && (previous_cmd != CMD_FACTORY_RESET))
        emit requestFactoryReset();
      break;
    case CMD_TOGGLE:
      res |= WthermoTouch;
      break;
    case CMD_DEVICE:
      if (state == Finish) {
        emit requestFactoryReset();
        break;
      }
      res |= WthermoA;
      if (cmd != previous_cmd) {
        device_cmd_count = 0;
        break;
      }

      if (thermo_type.getType() == ThermoDeviceType::Wthermo) {
        if (++device_cmd_count > 2) {
          res |= WthermoOut;
        }
      } else if (thermo_type.getType() == ThermoDeviceType::Dthermo) {
        if (++device_cmd_count > 2) {
          if ((res & WthermoTouch) == 0) {
            res |= WthermoTouch; // It's not touch, but out5
          }
        } else if (device_cmd_count == 2) {
          if ((res & WthermoOut) == 0) {
            res |= WthermoOut;
            emit requestConfigDthermoSummer(true);
            // device_cmd_count will be cleared since got config command
          }
        }
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0) {
        if (thermo_type.getType() == ThermoDeviceType::W1irth) {
          res |= W1irthProg;
        } else {
          res |= WthermoB;
        }
        if (isFinished(mti, res))
          emit requestFactoryReset();
      } else if (state == Finish) {
        emit requestMode(FtcInfo::NORMAL);
      }
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();  // signal
      }
      break;
    case CMD_FACTORY_RESET:
      if (state == Init) {
        res = 0;

        if (thermo_type.getType() == ThermoDeviceType::Wthermo) {
          emit requestConfigWthermo();      // Set to Air flow 2
        } else if (thermo_type.getType() == ThermoDeviceType::Dthermo) {
          emit requestConfigDthermoMode();  // Set to Air flow 4
        } else {
          setState(Run);                    // W-1IRTH does not need to set config
        }
      }
      break;
    case CMD_CONFIG:
    if ((state == Init) && (ftcinfo->getMessageType() == CONFIRMATION)){
        setState(Run);
      }
      break;
    default:
      break;
  }
  previous_cmd = cmd;

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testW4in()
{
  unsigned char cmd(ftcinfo->getCmd());
  static unsigned char previous_cmd(cmd);
  static int device_cmd_count(0);

  switch (cmd) {
    case CMD_TOGGLE:
      if (state == Finish) {
        emit requestFactoryReset();
        break;
      }
      res |= W4inTouch;
      break;
    case CMD_DEVICE:
      if (state == Finish) {
        emit requestTest(0);
        break;
      }
      if (cmd != previous_cmd) {
        device_cmd_count = 0;
        break;
      }
      if (++device_cmd_count > 0) {
        res |= W4inA;
      }
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    case CMD_FACTORY_RESET:
      if (state == Run) {
        res = 0;
      } else if (state == Finish) {
        previous_cmd = 0;
        emit requestTest(0);
      }
      break;
    default:
      break;
  }
  previous_cmd = cmd;

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testW1dim()
{
  unsigned char cmd(ftcinfo->getCmd());
  QByteArray param = ftcinfo->getParam();
  int level(0);
  int dimm_st(0);
  static bool f_up_pending(false);
  static bool f_dn_pending(false);

  switch (cmd) {
    case CMD_STATUS:
      dimm_st = static_cast<int>(param[1]);
      level = static_cast<int>(param[2]);
      if (state == Finish) {
        if ((dimm_st == UP)
            || ((dimm_st == STOP) && level))
          emit requestFactoryReset();
        break;
      }
      if (f_device_pending) {
        if (dimm_st == UP) {
          f_up_pending = true;
        } else if (dimm_st == DOWN) {
          f_dn_pending = true;
        } else {
          if (f_up_pending && level > 80)
            res |= W2onoffB;
          if (f_dn_pending && level < 20)
            res |= W2onoffA;
          f_up_pending = false;
          f_dn_pending = false;
          f_device_pending = false;
        }
      }
      break;
    case CMD_TOGGLE:
      res |= W2onoffTouch;
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (static_cast<int>(param[0]) == 0)
        res |= W2onoffProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testW1dimrgb()
{
  unsigned char cmd(ftcinfo->getCmd());
  QByteArray param = ftcinfo->getParam();
  int dimm_value(0);
  int dimm_v_st(0);
  int dimm_h_st(0);
  static bool f_up_pending(false);
  static bool f_dn_pending(false);
  static bool f_k3_touching(false);
  static bool f_hue_tuning(false);

  switch (cmd) {
    case CMD_STATUS:
      // Data from IndicateStatus() of rev0688-V1.7 
      // [7] = 0xFF
      // [8] = dimm_v_st 0:STOP 1:UP 2:DOWN
      // [9] = dimm_value
      // [10] = dimm_h_st
      // [11] = dimm_hue MSB
      // [12] = dimm_hue LSB
      // [13] = dimm_s_st
      // [14] = dimm_saturation
      dimm_v_st = static_cast<int>(param[1]);
      dimm_value = static_cast<int>(param[2]);
      dimm_h_st = static_cast<int>(param[3]);
      if (f_device_pending) {
        if (dimm_v_st == UP) {
          f_up_pending = true;
        } else if (dimm_v_st == DOWN) {
          f_dn_pending = true;
        } else {
          if (dimm_value > 80)
            res |= W2onoffB;
          else if (dimm_value < 20)
            res |= W2onoffA;
          if (f_up_pending) {
            qDebug() << "W-1DIMRGB has Key B pending";
            f_up_pending = false;
          }
          if (f_dn_pending) {
            qDebug() << "W-1DIMRGB has Key A pending";
            f_dn_pending = false;
          }
          f_device_pending = false;
        }
      } else {
        // Hue tuning by touching K3
        if (f_k3_touching) {
          qDebug() << "f_k3_touching" << dimm_h_st << f_hue_tuning;
          f_k3_touching = false;
          if (dimm_h_st == UP) {
            f_hue_tuning = true;
          } else {
            if (f_hue_tuning) {
              f_hue_tuning = false;
              res |= W2onoffTouch;
            }
          }
        }
      }

      if (state == Finish)
        emit requestFactoryReset();
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (static_cast<int>(param[0]) == 0)
        res |= W2onoffProg;
      break;
    case CMD_TOGGLE:
      if (static_cast<int>(param[0]) == 3)
        f_k3_touching = true;
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    f_up_pending = false;
    f_dn_pending = false;
    f_hue_tuning = false;
    setState(Finish);
  }
  emit result(res);
}

void Tester::testDdimrgb()
{
  unsigned char cmd(ftcinfo->getCmd());
  QByteArray param = ftcinfo->getParam();
  int dimm_value(0);
  int dimm_v_st(0);
  int dimm_h_st(0);
  int dimm_s_st(0);
  static bool f_up_pending(false);
  static bool f_dn_pending(false);
  static bool f_hue_tuning(false);
  static bool f_sat_tuning(false);

  if (state == Init) {
    f_up_pending = false;
    f_dn_pending = false;
    f_hue_tuning = false;
    f_sat_tuning = false;
  }

  switch (cmd) {
    case CMD_STATUS:
      // Data from IndicateStatus() of rev0688-V1.7 
      // [7 (0)] = 0xFF
      // [8 (1)] = dimm_v_st 0:STOP 1:UP 2:DOWN
      // [9] = dimm_value
      // [10] = dimm_h_st
      // [11] = dimm_hue MSB
      // [12] = dimm_hue LSB
      // [13] = dimm_s_st
      // [14] = dimm_saturation
      dimm_v_st = static_cast<int>(param[1]);
      dimm_value = static_cast<int>(param[2]);
      dimm_h_st = static_cast<int>(param[3]);
      dimm_s_st = static_cast<int>(param[6]);
      if (f_device_pending) {
        if (dimm_v_st == UP) {
          f_up_pending = true;
        } else if (dimm_v_st == DOWN) {
          f_dn_pending = true;
        }
        if (dimm_h_st == UP)
          f_hue_tuning = true;
        if (dimm_s_st == UP)
          f_sat_tuning = true;


        f_device_pending = false;
      } else {
      }

      if (f_up_pending && (dimm_v_st == STOP) && (dimm_value > 80))
        res |= DdimrgbA;
      if (f_dn_pending && (dimm_v_st == STOP) && (dimm_value < 20))
        res |= DdimrgbB;
      if (f_hue_tuning && (dimm_h_st == STOP))
        res |= DdimrgbC;
      if (f_sat_tuning && (dimm_s_st == STOP))
        res |= DdimrgbD;

      if (state == Finish)
        emit requestFactoryReset();
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    case CMD_TOGGLE_MASTER:
      if (static_cast<int>(param[0]) == 0)
        res |= DdimrgbProg;
      break;
    case CMD_FACTORY_RESET:
      if (state == Run) {
        res = 0;
        emit requestRGB(1, 90, 0, 0, 100); // RED color
      }
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testDgateway()
{
  unsigned char cmd(ftcinfo->getCmd());

  switch (cmd) {
    case CMD_DEVICE:
      if (state == Finish) {
        emit requestFactoryReset();
      } else {
        res |= DgatewayProg;
      }
      break;
    case CMD_TOGGLE:
    case CMD_SET:
    case CMD_RESET:
      if (state == Finish) {
        emit requestFactoryReset();
      } else {
        res |= DgatewayPW;
      }
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    default:
      break;
  }

  qDebug() << "res" << res;
  if (isFinished(mti, res)) {
    qDebug() << "testDgateway Finish";
    setState(Finish);
  }
  emit result(res);
}

void Tester::testW2vless()
{
  static QByteArray previous_output_status;
  unsigned char cmd = ftcinfo->getCmd();
  QByteArray output_status = ftcinfo->getParam().mid(1, 2);
  int mti_type = ftcinfo->getMti();

  switch (cmd) {
    case CMD_STATUS:
      if (previous_output_status.length() == 2) {
        if (output_status[0] != previous_output_status[0]) {
          if (f_device_pending) {
            f_device_pending = false;
            res |= W2vlessA;
          }
        }
        if (output_status[1] != previous_output_status[1]) {
          if (f_device_pending) {
            f_device_pending = false;
            res |= W2vlessB;
          }
        }
      }
      if (state == Finish) {
        if ((output_status.at(0) == 'S') || (output_status.at(1) == 'S')) {
          emit requestFactoryReset();
        }
      }
      previous_output_status = output_status;
      break;
    case CMD_TOGGLE:
      if (mti_type == MTI_2MOT) {
        res |= W2vlessTouch;
      } else if (mti_type == MTI_H3IN) {
        res |= W2vlessRF;
        emit requestVersion(); // TROG: To get all unit versions over FTC (W-2VLESS)
      }
      break;
    case CMD_DEVICE:
      f_device_pending = true;
      break;
    case CMD_TOGGLE_MASTER:
      if (ftcinfo->getParam()[0] == 0)
        res |= W2vlessProg;
      else if (state == Finish)
        emit requestMode(FtcInfo::NORMAL);
      break;
    case CMD_SOFT_VERSION:
      version = ftcinfo->getParam();
      if (state != Run) {
        emit requestFactoryReset();
        if (state == Init)
          setState(Run);
      }
      break;
    default:
      break;
  }

  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}

void Tester::testRc4brf()
{
  int button(ftcinfo->getParam()[0]);
  unsigned char cmd(ftcinfo->getCmd());
  QString status("Button: " + QString::number(button) + " ");

  switch (cmd) {
    case CMD_RESET:
      status += "RESET";
      if (button == 5) {
        res |= Rc4brfReset5;
      }
      break;
    case CMD_SET:
      status += "SET";
      if (button == 5) {
        res |= Rc4brfSet5;
      }
      break;
    case CMD_TOGGLE:
      status += "TOGGLE";
      switch (button) {
        case 1: res |= Rc4brfToggle1; break;
        case 2: res |= Rc4brfToggle2; break;
        case 3: res |= Rc4brfToggle3; break;
        case 4: res |= Rc4brfToggle4; break;
        default: break;
      }

      break;
    default:
      status += "ERROR";
      break;
  }

  emit message(status);
  if (isFinished(mti, res)) {
    setState(Finish);
  }
  emit result(res);
}
