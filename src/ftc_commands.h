// table of FTC commands
#define CMD_ANALOG 'A'               // 0x41
#define CMD_BLOCK 'B'                // 0x42
#define CMD_SCENARIO 'C'             // 0x43
#define CMD_DEVICE 'D'               // 0x44
#define CMD_ERASE_INPUT 'E'          // 0x45
#define CMD_INIT_OUTPUT 'I'          // 0x49
#define CMD_KEY_REMOTE 'K'           // 0x4B
#define CMD_TIMER 'L'                // 0x4C
#define CMD_MODE 'M'                 // 0x4D
#define CMD_NAME 'N'                 // 0x4E
#define CMD_DIRECT 'O'               // 0x4F
#define CMD_BABY_RX 'P'              // 0x50
#define CMD_BABY_DETECT 'Q'          // 0x51
#define CMD_RESET 'R'                // 0x52
#define CMD_SET 'S'                  // 0x53
#define CMD_TOGGLE 'T'               // 0x54
#define CMD_UPDATE_SCENARIO 'U'      // 0x55
#define CMD_CLIMA 'Y'                // 0x59
#define CMD_TOGGLE_MASTER 'Z'        // 0x5A
#define CMD_ADDRESS 'a'              // 0x61
#define CMD_BACKUP 'b'               // 0x62
#define CMD_CONFIG 'c'               // 0x63
#define CMD_DELAY 'd'                // 0x64
#define CMD_ERROR 'e'                // 0x65
#define CMD_FACTORY_RESET 'f'        // 0x66
#define CMD_BLINDS_AUTO 'h'          // 0x68
#define CMD_INTRUSION 'i'            // 0x69
#define CMD_TEMPERATURE 'k'          // 0x6B
#define CMD_LEDS_BUZZ 'l'            // 0x6C
#define CMD_BLINDS_MANUAL 'm'        // 0x6D
#define CMD_BLINDS_OPEN 'o'          // 0x6F
#define CMD_PROG_DATA 'p'            // 0x70
#define CMD_REPEAT 'r'               // 0x72
#define CMD_STATUS 's'               // 0x73
#define CMD_TIME_DATE 't'            // 0x74
#define CMD_UNIT_CMD 'u'             // 0x75
#define CMD_TEMPCTRL 'v'             // 0x76
#define CMD_SOFT_VERSION 'w'         // 0x77
#define CMD_OUTPUTSCTRL 'x'          // 0x78
#define CMD_TEST 'y'                 // 0x79
#define CMD_RESTORE 'z'              // 0x7A

#define MSG_SW_VERSION        0x00
#define MSG_TIME_DATE         0x01
#define MSG_USER_CODE         0x02
#define MSG_NEW_USER_ID       0x03
#define MSG_USER_DATA         0x04
#define MSG_SET_SCENARIO      0x05
#define MSG_ABORT             0x06
#define MSG_SCENARIO          0x07
#define MSG_SERVICE_CODE      0x08
#define MSG_MASTER_CODE       0x09
#define MSG_ZONE_CONFIG       0x0A
#define MSG_USER_INFO         0x0B
#define MSG_ALARM_ROUTINE     0x0C
#define MSG_KEYB_ACK          0x0D   //was keyb_lock
#define MSG_ALARM_STATUS      0x0E
#define MSG_EXIT_TIME         0x0F
#define MSG_ENTRY_TIME        0x10
#define MSG_ZONE_STATUS       0x11
#define MSG_INTERNAL_SIREN_ON 0x12
#define MSG_USERS_LIST        0x13
#define MSG_ERASE_USER        0x14
#define MSG_EVENT_RECORDER    0x15
#define MSG_RECORD            0x16
#define MSG_USER_TEMPLATE     0x17
#define MSG_HARDWARE          0x18
#define MSG_FACTORY_INIT      0x19
#define MSG_WALK_TEST         0x1A
#define MSG_BYPASS            0x1B
#define MSG_ALARM_TIME        0x1C
#define MSG_POWER             0x1D
#define MSG_TELEPHONE         0x1E
#define MSG_OUTPUT_NAME       0x1F
#define MSG_OUTPUT_CONTROL    0x20
#define MSG_ARM_CONTROL       0x21
#define MSG_KEYB_LIGHTS       0x22
#define MSG_ACCESS_CODE       0x23
#define MSG_SESSION           0x24
#define MSG_LANGUAGE          0x25
#define MSG_SERIAL_NR         0x26
#define MSG_KEYB_CHECK        0x27
#define MSG_TAMPER            0x28
#define MSG_MESSAGE           0x29
#define MSG_TEST_MODE         0x2A
#define MSG_USER_VALID        0x2B
#define MSG_STATION_NR        0x2C
#define MSG_NR_RINGS          0x2D
#define MSG_RESET_MCODE       0x2E
#define MSG_CODE_USAGE        0x2F
#define MSG_POWER_STATUS      0x30
#define MSG_SERV_DISPLAY      0x31
#define MSG_EXIT_SERVICE      0x32
#define MSG_REMOTE_CALL       0x33
#define MSG_CID_CODES         0x34
#define MSG_BLOCK             0x35
#define MSG_CALL_BACK         0x36   //REQ toggles CALL BACK #define MSG_PROG_DATA         0x36
#define MSG_SIGN_OF_LIFE      0x37
#define MSG_ADDRESS           0x38
//#define MSG_NAME              0x39
#define MSG_SIREN_MODE        0x39   // REQ toggles SIREN mode
#define MSG_OUTPUTS           0x3A
#define MSG_CONFIGURATION     0x3B
#define MSG_AUTO_NIGHT        0x3C   // REQ toggles AUTO_NIGHT mode
#define MSG_SIMULATOR         0x3D   // REQ toggles SIMULATOR mode
#define MSG_DURESS_CODE       0x3E
#define MSG_CALL_HELP         0x3F

#define CMD_COPY_INPUT        0x40

//new commands
#define MSG_FAULT_CONDITIONS   0x80
#define MSG_TEST_REPORT        0x82
#define MSG_BATT_VOLTAGE       0x84
#define MSG_ZONE_SUPERVISION   0x85
#define MSG_OUTPUTS_DIAGNOSTIC 0x86
#define MSG_NEW_EVENT_RECORDER 0x87
#define MSG_NEW_USERS_LIST     0x88
#define MSG_SERVICE_CODE_AUTHORIZE 0x89

//special command 
//relay latch and unlatch times
#define CMD_RELAY_TIMES        0x90


//parameters for the error command
//  1 ... 127 reserved to output numbers
#define E_CMD_WRONG_PARAMETRERS   0x80
#define E_TEMP_MISSING            0x90

