#include "printreport.h"
#include <QDebug>

PrintReport::PrintReport(QWidget *parent) :
    QDialog(parent)
{
  createGroupBoxDate();
  createGroupBoxProduct();
  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(groupBoxDate);
  mainLayout->addWidget(groupBoxProduct);
  setLayout(mainLayout);
  setWindowTitle(tr("Print Report"));
  resize(300, 200);

  connect(pushButtonExport, SIGNAL(clicked()), SLOT(onExport()));
  connect(pushButtonCancel, SIGNAL(clicked()), SLOT(onCancel()));
}

PrintReport::~PrintReport()
{
}

void PrintReport::createGroupBoxDate()
{
  groupBoxDate = new QGroupBox;
  labelStartDate = new QLabel("Start Date:");
  labelFinishDate = new QLabel("Finish Date:");
  QVBoxLayout *dateLayout = new QVBoxLayout;
  calendarStart = new QCalendarWidget;
  calendarFinish = new QCalendarWidget;

  dateLayout->addWidget(labelStartDate);
  dateLayout->addWidget(calendarStart);
  dateLayout->addWidget(labelFinishDate);
  dateLayout->addWidget(calendarFinish);

  groupBoxDate->setLayout(dateLayout);
}

void PrintReport::createGroupBoxProduct()
{
  groupBoxProduct = new QGroupBox;
  labelMti = new QLabel("Product:");
  pushButtonExport = new QPushButton(tr("Export"));
  pushButtonCancel = new QPushButton(tr("Cancel"));
  radioButtonAll = new QRadioButton("All", this);
  radioButtonPass = new QRadioButton("Pass", this);
  radioButtonFail = new QRadioButton("Fail", this);
  radioButtonLast = new QRadioButton("Last", this);
  QVBoxLayout *productLayout = new QVBoxLayout;
  QHBoxLayout *mtiLayout = new QHBoxLayout;
  QHBoxLayout *filterLayout = new QHBoxLayout;
  QHBoxLayout *actionLayout = new QHBoxLayout;
  createMtiList();

  radioButtonAll->setChecked(true);
  mtiLayout->addWidget(labelMti);
  mtiLayout->addWidget(comboBoxMti);
  filterLayout->addWidget(radioButtonAll);
  filterLayout->addWidget(radioButtonPass);
  filterLayout->addWidget(radioButtonFail);
  filterLayout->addWidget(radioButtonLast);
  actionLayout->addWidget(pushButtonExport);
  actionLayout->addWidget(pushButtonCancel);

  productLayout->addLayout(mtiLayout);
  productLayout->addLayout(filterLayout);
  productLayout->addLayout(actionLayout);
  groupBoxProduct->setLayout(productLayout);
}

void PrintReport::createMtiList()
{
  comboBoxMti = new QComboBox;
  comboBoxMti->addItem(tr("880-xxxx All Products"), tr(""));
  
  comboBoxMti->addItem(tr("880-0001 W-2ONOFF"),
                       QString::number(MTI_2OUT, 16).toUpper());
  comboBoxMti->addItem(tr("880-0003 D-1DIM010V"),
                       QString::number(MTI_1DIM010V, 16).toUpper());
  comboBoxMti->addItem(tr("880-0004 D-1DIM"),
                       QString::number(MTI_1DIM, 16).toUpper());
  comboBoxMti->addItem(tr("880-0005 D-GATEWAYIP"),
                       QString::number(MTI_FTC2E, 16).toUpper());
  comboBoxMti->addItem(tr("880-0009 D-8ONOFF"),
                       QString::number(MTI_8OUT, 16).toUpper());
  comboBoxMti->addItem(tr("880-0010 D-4SHUTT"),
                       QString::number(MTI_D4SHU, 16).toUpper());
  comboBoxMti->addItem(tr("880-0012 D-THERMO"),
                       QString::number(MTI_TERMO, 16).toUpper());
  comboBoxMti->addItem(tr("880-0016 W-1DIM010V"),
                       QString::number(MTI_1DIM010V, 16).toUpper());
  comboBoxMti->addItem(tr("880-0017 W-1DIM"),
                       QString::number(MTI_1DIM, 16).toUpper());
  comboBoxMti->addItem(tr("880-0019 W-FMUNIT2RBT"),
                       QString::number(MTI_FM2AU, 16).toUpper());
  comboBoxMti->addItem(tr("880-0020 W-INPKEYB"),
                       QString::number(MTI_4IN, 16).toUpper());
  comboBoxMti->addItem(tr("880-0021 W-1SHUTT"),
                       QString::number(MTI_1SHU, 16).toUpper());
  comboBoxMti->addItem(tr("880-0022 W-2VLESS"),
                       QString::number(MTI_2MOT, 16).toUpper());
  comboBoxMti->addItem(tr("880-0023 W-THERMO"),
                       QString::number(MTI_TERMO, 16).toUpper());
  comboBoxMti->addItem(tr("880-0024 W-1DIMRGB"),
                       QString::number(MTI_DIMRGB, 16).toUpper());
  comboBoxMti->addItem(tr("880-0038 D-BUSSUPIP"),
                       QString::number(MTI_FTC2E, 16).toUpper());
  comboBoxMti->addItem(tr("880-0039 RC-4BRF"),
                       QString::number(MTI_H3IN, 16).toUpper());
  comboBoxMti->addItem(tr("880-0042 W-4SEC"),
                       QString::number(MTI_4SEC, 16).toUpper());
  comboBoxMti->addItem(tr("880-0053 D-DIMRGB"),
                       QString::number(MTI_DIMRGB, 16).toUpper());
  comboBoxMti->addItem(tr("880-0055 W-2ONOFF-H"),
                       QString::number(MTI_2OUT, 16).toUpper());
  comboBoxMti->addItem(tr("880-0056 W-1SHUTT-H"),
                       QString::number(MTI_1SHU, 16).toUpper());
  comboBoxMti->addItem(tr("880-0057 D-8ONOFF-H"),
                       QString::number(MTI_8OUT, 16).toUpper());
  comboBoxMti->addItem(tr("880-0010 D-4SHUTT-H"),
                       QString::number(MTI_D4SHU, 16).toUpper());

}

void PrintReport::onExport()
{
  QDate start = calendarStart->selectedDate();
  QDate finish = calendarFinish->selectedDate();
  QString mti = comboBoxMti->itemData(comboBoxMti->currentIndex()).toString();
  char filter;

  finish = finish.addDays(1);
  if (radioButtonAll->isChecked()) {
    filter = 'a';
  } else if (radioButtonPass->isChecked()) {
    filter = 'p';
  } else if (radioButtonFail->isChecked()) {
    filter = 'f';
  } else {
    filter = 'l'; // Last result of each units
  }
  emit report(start.toString("yyyy-MM-dd"), finish.toString("yyyy-MM-dd"),
              mti, comboBoxMti->currentText(), filter);
}

void PrintReport::onCancel()
{
  hide();
}

