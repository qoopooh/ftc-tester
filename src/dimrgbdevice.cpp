﻿#include "dimrgbdevice.h"

DimRgbDevice::DimRgbDevice(QWidget *parent) :
  QDialog(parent)
{
  QPushButton *acceptButton = new QPushButton(tr("OK"));
  QVBoxLayout *grid = new QVBoxLayout;
  grid->addWidget(createSelectionGroup());
  grid->addWidget(acceptButton);
  setLayout(grid);

  setWindowTitle(QString::fromUtf8("กรุณาเลือก 1 ผลิตภัณฑ์"));
  resize(240, 120);

  QObject::connect(acceptButton, SIGNAL(clicked()), this, SLOT(sendResult()));
}

DimRgbDevice::~DimRgbDevice()
{
}

QGroupBox * DimRgbDevice::createSelectionGroup()
{
  QGroupBox *groupBox = new QGroupBox(tr("DIMRGB Devices:"));

  QRadioButton *radio1 = new QRadioButton(tr("&W-1DIMRGB"), groupBox);
  QRadioButton *radio2 = new QRadioButton(tr("&D-DIMRGB"), groupBox);
  connect(radio1, SIGNAL(toggled(bool)), this, SLOT(setRadio1(bool)));
  connect(radio2, SIGNAL(toggled(bool)), this, SLOT(setRadio2(bool)));

  radio1->setChecked(true);

  QVBoxLayout *vbox = new QVBoxLayout;
  vbox->addWidget(radio1);
  vbox->addWidget(radio2);
  vbox->addStretch(1);
  groupBox->setLayout(vbox);

  return groupBox;
}

void DimRgbDevice::setRadio1(bool toggle) {
  if (toggle)
    setResult(DimRgbDeviceType::W1dimrgb);
}

void DimRgbDevice::setRadio2(bool toggle) {
  if (toggle)
    setResult(DimRgbDeviceType::Ddimrgb);
}

void DimRgbDevice::sendResult() {
  if (result() == 0) {
    setResult(DimRgbDeviceType::W1dimrgb);
  }
  done(result());
}

