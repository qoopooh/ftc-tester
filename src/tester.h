#ifndef TESTER_H
#define TESTER_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include "ftc232.h"
#include "thermodevice.h"
#include "thermodevicetype.h"
#include "dimrgbdevicetype.h"
#include "dimrgbdevice.h"


class Tester : public QObject
{
  Q_OBJECT
public:
  explicit Tester(QObject *parent = 0);
  ~Tester();
  bool isPassTest(const QString &sn, const int res);

  void initTest(const QString &sn, FtcInfo &);
  bool isRunning();
  bool isCompleted();
  bool isVirtualButton(QString, QString);
  quint16 getResult();
  QString getSofwareVersion();
  void exec(const FtcInfo &);
  void setPassedUnit(const QString &sn);
  void clearUnit();
  void strengthToggle();
  void strengthClear();
  void setThermoType(ThermoDeviceType::UnitType);
  void setDimrgbType(DimRgbDeviceType::UnitType);

signals:
  void finish(quint32);
  void result(quint16);
  void requestSoftwareVersion();
  void requestFactoryReset();
  void requestStatus();
  void requestToggle(int);
  void requestSetOutput(int);
  void requestResetOutput(int);
  void requestMode(int);
  void requestConfigWthermo();
  void requestConfigDthermoMode();
  void requestConfigDthermoSummer(bool);
  void requestTest(int);
  void requestVersion();
  void requestRGB(const int,const int,const int,const int,const int);
  void message(QString);

public slots:

protected:
  typedef enum {
    Init,
    Run,
    Finish
  } TestState;
  typedef enum {
    Stop,
    Up,
    Down,
    Unknown
  } MoveState;
  typedef enum {
    W2onoffA     = 0x01,
    W2onoffB     = 0x02,
    W2onoffTouch = 0x04,
    W2onoffProg  = 0x08,
    W2onoffPass  = 0x0f
  } W2onoff;
  typedef enum {
    D8onoff1     = 0x0001,
    D8onoff2     = 0x0002,
    D8onoff3     = 0x0004,
    D8onoff4     = 0x0008,
    D8onoff5     = 0x0010,
    D8onoff6     = 0x0020,
    D8onoff7     = 0x0040,
    D8onoff8     = 0x0080,
    D8onoffProg  = 0x0100,
    D8onoffIn1   = 0x0200,
    D8onoffIn2   = 0x0400,
    D8onoffIn3   = 0x0800,
    D8onoffPass  = 0x0fff
  } D8onoff;
  typedef enum {
    WthermoA     = 0x01,
    WthermoB     = 0x02,
    WthermoTouch = 0x04,
    WthermoOut   = 0x08,
    WthermoPass  = 0x0f
  } Wthermo;
  typedef enum {
    W1irthProg    = 0x01,
    W1irthPass    = 0x01
  } W1irth;
  typedef enum {
    W4inA         = 0x01,
    W4inTouch     = 0x02,
    W4inPass      = 0x03
  } W4in;
  typedef enum {
    DgatewayProg  = 0x01,
    DgatewayPW    = 0x02,
    DgatewayPass  = 0x03
  } Dgateway;
  typedef enum {
    W2vlessA      = 0x01,
    W2vlessB      = 0x02,
    W2vlessTouch  = 0x04,
    W2vlessProg   = 0x08,
    W2vlessRF     = 0x10,
    W2vlessPass   = 0x1f
  } W2vless;
  typedef enum {
    Rc4brfToggle1 = 0x01,
    Rc4brfToggle2 = 0x02,
    Rc4brfToggle3 = 0x04,
    Rc4brfToggle4 = 0x08,
    Rc4brfReset5  = 0x10,
    Rc4brfSet5    = 0x20,
    Rc4brfPass    = 0x3F
  } Rc4brf;
  typedef enum {
    DdimrgbA      = 0x01,
    DdimrgbB      = 0x02,
    DdimrgbC      = 0x04,
    DdimrgbD      = 0x08,
    DdimrgbProg   = 0x10,
    DdimrgbPass   = 0x17
  } Ddimrgb;
  
  typedef enum {
    W1dimrgbPass  = 0x0f
  } W1dimrgb;

private slots:
  void onTimeout();

private:
  bool setState(TestState);
  MoveState toMoveState(quint8);
  bool isFinished(const unsigned char mti, const quint16 res);
  void testW2onoff();
  void testW1shutter();
  void testD8onoff();
  void testD4shutter();
  void testWthermo();
  void testW4in();
  void testW1dim();
  void testW1dimrgb();
  void testDdimrgb();
  void testDgateway();
  void testW2vless();
  void testRc4brf();

  FtcInfo *ftcinfo;
  QString unitname;
  QString version;
  TestState state;
  QTimer *timer;
  quint16 res;
  unsigned char mti;
  bool f_device_pending;
  bool f_strength;

  ThermoDeviceType thermo_type;
  DimRgbDeviceType dimrgb_type;

};

#endif // TESTER_H

