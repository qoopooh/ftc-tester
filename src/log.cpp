#include "log.h"

Log::Log(QObject *parent) :
    QObject(parent)
{
  filename = "out.dat";
  logfile = new QFile(filename);
}

void Log::logData(const QByteArray &ba)
{
  logPacketReading(ba);
}
void Log::logPacketReading (const QByteArray &ba)
{
  if (!logfile->open(QIODevice::Append))
    return;
  date = QDate::currentDate ();
  logfile->write (date.toString ().toLatin1());
  logfile->write (" ");
  time = QTime::currentTime ();
  logfile->write (time.toString ().toLatin1());
  logfile->write (", ");
  logfile->write (ba);
  logfile->write ("\r");
  logfile->close ();
}

