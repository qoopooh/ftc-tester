#ifndef FTC232_H
#define FTC232_H

#include <QThread>
#include <qextserialport.h>
#include <qextserialenumerator.h>

#include "ftcinfo.h"

enum SpecialByte
{
  Mask1 = 0x5D,
  Mask2 = 0x5E,
  Mask3 = 0x5F,
  ByteStart = 0x7D,
  ByteMask = 0x7E,
  ByteEnd = 0x7F
};

class Ftc232 : public QObject
{
  Q_OBJECT
public:
  explicit Ftc232(QObject *parent = 0);
  ~Ftc232();

  static QList<QString> discovery();
  FtcInfo &getFtcinfo();
  void open();
  void close();

signals:
  void message(QString);
  void sn(QString);
  void ftcStatus(QString);
  void error(QString);

public slots:
  void setPortname(const QString);
  void sendCommand(const unsigned char cmd, const QByteArray param=NULL);
  void sendFactoryReset();
  void sendSoftwareVersion();
  void sendStatus();
  void sendToggle(const int out=0xFF);
  void sendSetOutput(const int);
  void sendResetOutput(const int);
  void sendMode(const int);
  void sendConfigWthermo();
  void sendConfigDthermoMode();
  void sendConfigDthermoSummer(bool);
  void sendTest(const int);
  void sendAnalogRGB(const int out, const int dimm, const int hue_h,
      const int hue_l, const int sat);

protected:
//  virtual void run();

private Q_SLOTS:
  void onReadyRead();
  void onFtcTime(const QString);
  void onFTCTemperature(const QString);

private:
  void extractMessage(const QByteArray &);
  bool checksum(const QByteArray &);
  quint8 calChecksum(const QByteArray &);
  void writeFtcUart(const QByteArray &);
  QByteArray toFtcMessage(const QByteArray &);
  QByteArray fromFtcMessage(const QByteArray &);

  QextSerialPort *port;
  QString portname;
  QByteArray recv;
  FtcInfo *info;
  bool f_writable;
};

#endif // FTC232_H
