#ifndef DIMRGBDEVICETYPE_H
#define DIMRGBDEVICETYPE_H

#include <QObject>
#include <QString>

class DimRgbDeviceType : public QObject
{
    Q_OBJECT
public:
    //explicit DimRgbDeviceType(QObject *parent = 0);
    typedef enum {
      None,
      W1dimrgb,
      Ddimrgb
    } UnitType;
    DimRgbDeviceType();
    DimRgbDeviceType(UnitType);

    void setType(UnitType);
    UnitType getType();

    QString toString();
    QString k_w1Dimrgb;
    QString k_dDimrgb;

private:
    UnitType m_type;

signals:

public slots:

};

#endif // DIMRGBDEVICETYPE_H
