#include "dimrgbdevicetype.h"

//DimRgbDeviceType::DimRgbDeviceType(QObject *parent) :
//    QObject(parent)
//{
//}


DimRgbDeviceType::DimRgbDeviceType()
  : k_w1Dimrgb("W-1DIMRGB"),
    k_dDimrgb("D-DIMRGB")
{
  setType(None);
}

void DimRgbDeviceType::setType(UnitType type)
{
  m_type = type;
}

DimRgbDeviceType::UnitType DimRgbDeviceType::getType()
{
  return m_type;
}

QString DimRgbDeviceType::toString()
{
  QString str("-");

  switch (m_type) {
    case W1dimrgb:
      str = k_w1Dimrgb;
      break;
    case Ddimrgb:
      str = k_dDimrgb;
      break;
    default:
      break;
  }
  return str;
}

