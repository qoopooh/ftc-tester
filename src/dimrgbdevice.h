﻿#ifndef DIMRGBDEVICE_H
#define DIMRGBDEVICE_H

#include <QDialog>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QPushButton>

#include "dimrgbdevicetype.h"

class DimRgbDevice : public QDialog
{
  Q_OBJECT

public:

  explicit DimRgbDevice(QWidget *parent = 0);
  ~DimRgbDevice();

  typedef enum {
    None,
    W1dimrgb,
    Ddimrgb
  } UnitType;

private slots:
  void setRadio1(bool);
  void setRadio2(bool);
  void sendResult();

private:
  QGroupBox *createSelectionGroup();
};

#endif // DIMRGBDEVICE_H
