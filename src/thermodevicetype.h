#ifndef THERMODEVICETYPE_H
#define THERMODEVICETYPE_H

#include <QObject>
#include <QString>

class ThermoDeviceType : public QObject
{
Q_OBJECT
public:
  typedef enum {
    None,
    Wthermo,
    Dthermo,
    W1irth
  } UnitType;

  ThermoDeviceType();
  ThermoDeviceType(UnitType);

  void setType(UnitType);
  UnitType getType();

  QString toString();
  QString k_wthermo;
  QString k_dthermo;
  QString k_w1irth;

private:
  UnitType m_type;

};

#endif // THERMODEVICETYPE_H
