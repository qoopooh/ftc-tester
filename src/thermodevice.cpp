#include "thermodevice.h"

ThermoDevice::ThermoDevice(QWidget *parent) :
  QDialog(parent)
{
  QPushButton *acceptButton = new QPushButton(tr("OK"));
  QVBoxLayout *grid = new QVBoxLayout;
  grid->addWidget(createSelectionGroup());
  grid->addWidget(acceptButton);
  setLayout(grid);

  setWindowTitle(QString::fromUtf8("กรุณาเลือก 1 ผลิตภัณฑ์"));
  resize(240, 180);

  QObject::connect(acceptButton, SIGNAL(clicked()), this, SLOT(sendResult()));
}

ThermoDevice::~ThermoDevice()
{
}

QGroupBox * ThermoDevice::createSelectionGroup()
{
  QGroupBox *groupBox = new QGroupBox(tr("THERMO Devices:"));

  QRadioButton *radio1 = new QRadioButton(tr("&W-THERMO"), groupBox);
  QRadioButton *radio2 = new QRadioButton(tr("&D-THERMO"), groupBox);
  QRadioButton *radio3 = new QRadioButton(tr("W-&1IRTH"), groupBox);
  connect(radio1, SIGNAL(toggled(bool)), this, SLOT(setRadio1(bool)));
  connect(radio2, SIGNAL(toggled(bool)), this, SLOT(setRadio2(bool)));
  connect(radio3, SIGNAL(toggled(bool)), this, SLOT(setRadio3(bool)));

  radio1->setChecked(true);

  QVBoxLayout *vbox = new QVBoxLayout;
  vbox->addWidget(radio1);
  vbox->addWidget(radio2);
  vbox->addWidget(radio3);
  vbox->addStretch(1);
  groupBox->setLayout(vbox);

  return groupBox;
}

void ThermoDevice::setRadio1(bool toggle) {
  if (toggle)
    setResult(ThermoDeviceType::Wthermo);
}

void ThermoDevice::setRadio2(bool toggle) {
  if (toggle)
    setResult(ThermoDeviceType::Dthermo);
}

void ThermoDevice::setRadio3(bool toggle) {
  if (toggle)
    setResult(ThermoDeviceType::W1irth);
}

void ThermoDevice::sendResult() {
  if (result() == 0) {
    setResult(ThermoDeviceType::Wthermo);
  }
  done(result());//(Done)->return section item to parent class
}

