#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <QFile>
#include <QFileInfo>

#include "ftc232.h"
#include "database.h"
#include "tester.h"
#include "printreport.h"
#include "thermodevice.h"
#include "thermodevicetype.h"
#include "dimrgbdevice.h"

namespace Ui {
  class MainWindow;
}

class Tester;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:

  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

protected:
  virtual void resizeEvent(QResizeEvent *event);

private slots:
  void onTimeout();
  void onSerialNumber(const QString);
  void onTestResult(const quint16);
  void onDatabaseError(const int, const QString);
  void onFtcError(const QString);
  void onReport(const QString start, const QString finish,
                const QString mti, const QString header, const char);
  void onThermoDialog(int);
  void onDimrgbDialog(int);
  void on_pushButtonRefresh_clicked();
  void on_checkBoxConnect_toggled(bool checked);
  void on_pushButtonAddress_clicked();
  void on_pushButtonClear_clicked();
  void on_pushButtonFactory_clicked();
  void on_pushButtonVersion_clicked();
  void on_pushButtonStatus_clicked();
  void on_pushButtonFull_clicked();
  void on_pushButtonReport_clicked();
  void on_pushButtonStrength_clicked();

private:
  void searchFtc232();
  void setGroupDisabled(bool checked);
  bool isReservedProductLot(int lot);
  bool isFirstThermoDevice(int mti);
  bool isFirstDimrgbDevice(int mti);
  void stopTest();
  void startTest();

  Ui::MainWindow *ui;
  Ftc232 *ftc;
  Database *db;
  Tester *test;
  PrintReport *printdialog;
  ThermoDevice *thermodialog;
  ThermoDeviceType thermo_type;
  DimRgbDevice *dimrgbdialog;
  DimRgbDeviceType dimrgb_type;
  bool f_rf_check;
  bool f_rf_rc_4brf;
};

#endif // MAINWINDOW_H
