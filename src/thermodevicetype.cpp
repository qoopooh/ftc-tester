#include "thermodevicetype.h"

ThermoDeviceType::ThermoDeviceType()
  : k_wthermo("W-THERMO"),
    k_dthermo("D-THERMO"),
    k_w1irth("W-1IRTH")
{
  setType(None);
}

void ThermoDeviceType::setType(UnitType type)
{
  m_type = type;
}

ThermoDeviceType::UnitType ThermoDeviceType::getType()
{
  return m_type;
}

QString ThermoDeviceType::toString()
{
  QString str("-");

  switch (m_type) {
    case Wthermo:
      str = k_wthermo;
      break;
    case Dthermo:
      str = k_dthermo;
      break;
    case W1irth:
      str = k_w1irth;
      break;
    default:
      break;
  }

  return str;
}
