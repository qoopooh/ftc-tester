#ifndef FTCINFO_H
#define FTCINFO_H

#include <QObject>
#include "ftc_commands.h"
#include "MTI.h"

// source addresses
#define SRC_ALL 0x00
#define SRC_SECURITY (0x20)
#define SRC_AUTOMATION (0x40)
#define SRC_SEC_AUTO  (0x60)
#define SRC_SCREEN (0x80)
#define SRC_MODEM (0xc0)
#define SRC_RS232_TD (0xe0)
// destination addresses
#define DST_ALL 0x00
#define DST_SECURITY (0x04)
#define DST_AUTOMATION (0x08)
#define DST_SEC_AUTO  (0x0c)
#define DST_SCREEN (0x10)
#define DST_MODEM (0x18)
#define DST_RS232_TD (0x1c)
// message types
#define ENQUIRY 0x00
#define INDICATION 0x01
#define REQUEST 0x02
#define CONFIRMATION 0x03

// W-1SHUTT
#define STOP 0
#define UP   1
#define DOWN 2
// W-THERMO, D-THERMO
#define CNF_CYCLE       3
#define CNF_RELAY_MODE  4

#define UNIT_AIRFLOW2   3
#define UNIT_AIRFLOW4   4

class FtcInfo : public QObject
{
  Q_OBJECT
public:
  explicit FtcInfo(QObject *parent = 0);
  ~FtcInfo();

  void setInfo(const QByteArray &ba);
  unsigned char getMessageType();
  unsigned char getCmd();
  int getMti();
  int getProductLot();
  int getSerial();
  /**
    * return HEX UPPER CASE string (8 charactors) of MTI address
    */
  const QString getAddress();
  /**
    * return 4 bytes (hex) of MTI address
    */
  const QByteArray getMtiAddress();
  const QByteArray getParam();
  bool isCmd8Set();

  typedef enum {
    NORMAL,
    PROG,
    ERASE
  } Mode;

signals:
  void address(QString);
  void time(QString);
  void temperature(QString);
  void event(QString);

protected:

private:
  void setAddress(const QByteArray &ba);
  void setParam(const QByteArray &ba);
  void emitStatus();
  const QString toDegree();
  const QString toTime();
  void debugCommand();

  unsigned char msg_type;
  unsigned char cmd;
  unsigned char mti;
  int product_lot;
  int serial;
  QByteArray addr;
  QByteArray param;
  bool f_addr_set;
  bool f_cmd_bit8;
};

#endif // FTCINFO_H
