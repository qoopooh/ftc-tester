# Domotics Tester
This software is used as jig tester for electronics production line.
Source code size is around 5K LOC. I recommended to open it with Qt Creator.

## Platform
Mainly be used on windows/linux machines. You can try to compile it on OSX as well but GUI does seems to be nice.

## Features
What kind of things you can see in the sources:

  * Serial port connection with binary streaming
  * MySQL database connection
  * HTML report generator

In **doc** folder, you can see hardware wiring diagram. Test procedure are excluded from this repository.

License
----

MIT
